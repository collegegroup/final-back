package com.proyecto.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.proyecto.api.dataTypes.DtMensaje;

@Entity
@Table(name = "mensajes", schema = "public")
public class Mensaje {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int _id;

    @ManyToOne
    @JoinColumn(name = "_usuarioid")
    private Usuario usuario;

    @ManyToOne
    @JoinColumn(name = "_foroid")
    @JsonIgnoreProperties("mensajes")
    private Foro foro;

    @Column(name = "titulo")
    private String titulo;

    @Column(name = "contenido")
    private String contenido;


    public int get_id() {
        return _id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Foro getForo() {
        return foro;
    }

    public void setForo(Foro foro) {
        this.foro = foro;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public Mensaje() {
    }

    public Mensaje(int _id, Usuario usuario, Foro foro, String titulo, String contenido) {
        this._id = _id;
        this.usuario = usuario;
        this.foro = foro;
        this.titulo = titulo;
        this.contenido = contenido;
    }

    public DtMensaje getDataType(){
        DtMensaje toReturn = new DtMensaje();

        toReturn.setContenido(contenido);
        toReturn.setForo(foro._id);
        toReturn.setTitulo(titulo);
        toReturn.setUsuario(usuario.getDataTypeBase());
        toReturn.set_id(_id);

        return toReturn;
    }
    
}
