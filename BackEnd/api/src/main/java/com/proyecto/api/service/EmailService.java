package com.proyecto.api.service;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.proyecto.api.model.Email;

import org.springframework.stereotype.Service;

@Service
public class EmailService {

    public static boolean sendEmail(Email e) {    
       
        String subject = e.getSubject();
        String body = e.getBody();        
        String to = e.getTo();
        String from = e.getFrom();
        String password =e.getPassword();


        Properties props = new Properties();  
        props.setProperty("mail.transport.protocol", "smtp");     
        props.setProperty("mail.host", "smtp.gmail.com");  
        props.put("mail.smtp.auth", "true");  
        props.put("mail.smtp.port", "465");  
        props.put("mail.debug", "true");  
        props.put("mail.smtp.starttls.enable","true");
        props.put("mail.smtp.starttls.required","true");
        props.put("mail.smtp.socketFactory.port", "465");  
        props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");  
        props.put("mail.smtp.socketFactory.fallback", "false");  
        Session session = Session.getDefaultInstance(props,  
        new javax.mail.Authenticator() {
           protected PasswordAuthentication getPasswordAuthentication() {  
           return new PasswordAuthentication(from,password);  
       }  
       });  

      


        try {
        	 Transport transport = session.getTransport();  
             InternetAddress addressFrom = new InternetAddress(from); 
             
            MimeMessage message = new MimeMessage(session);
            message.setFrom(addressFrom);
            
            InternetAddress[] iAdressArray = InternetAddress.parse(to);
            message.setRecipients(Message.RecipientType.BCC, iAdressArray);

            message.setSubject(subject);

            message.setContent(body,"text/html");

            transport.connect();  
            Transport.send(message);  
            transport.close();

            return true;

          } catch (MessagingException mex) {
                mex.printStackTrace();
                return false;
          }
       }
}
