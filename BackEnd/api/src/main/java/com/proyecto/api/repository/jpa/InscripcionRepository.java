package com.proyecto.api.repository.jpa;

import java.util.List;

import com.proyecto.api.model.Inscripcion;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface InscripcionRepository extends JpaRepository<Inscripcion,String>{
    Inscripcion findBy_id(int _id);

    @Query(
        value = "SELECT * FROM inscripciones AS i WHERE i._cursoid = :idCurso", 
        nativeQuery = true)
    List<Inscripcion> getInscripcionesDeCurso(@Param("idCurso") int idCurso);

    @Query(
        value = "SELECT * FROM inscripciones AS i WHERE i._estudianteid = :idEstudiante", 
        nativeQuery = true)
    List<Inscripcion> getInscripcionesDeEstudiante(@Param("idEstudiante") int idEstudiante);

}
