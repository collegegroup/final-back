package com.proyecto.api.service;

import java.util.ArrayList;
import java.util.List;

import com.proyecto.api.dataTypes.DtMensaje;
import com.proyecto.api.model.Mensaje;
import com.proyecto.api.repository.jpa.ForoRepository;
import com.proyecto.api.repository.jpa.MensajeRepository;
import com.proyecto.api.repository.jpa.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MensajeService {

    @Autowired
    private MensajeRepository repository;
    @Autowired
    private ForoRepository frepository;
    @Autowired
    private UsuarioRepository urepository;

    public List<DtMensaje> getAllMensajes() {
        List<DtMensaje> retorno = new ArrayList<>();
        List<Mensaje> mensajes = repository.findAll();
        for(Mensaje m : mensajes){
            retorno.add(m.getDataType());
        }
        return retorno;
    }

    public DtMensaje getMensajeById(int id) {
        return repository.findBy_id(id).getDataType();
    }

    public DtMensaje modifyMensajeById(int id, Mensaje mensaje) {
        var m = repository.findBy_id(id);
        m.setContenido(mensaje.getContenido());
        m.setTitulo(mensaje.getTitulo());
        return repository.save(m).getDataType();
    }

    public DtMensaje altaMensaje(Mensaje mensaje, Integer foroId, Integer usuarioId) {
        var foro = frepository.findBy_id(foroId);
        foro.addMensaje(mensaje);
        mensaje.setForo(foro);

        var usuario = urepository.findBy_id(usuarioId);
        mensaje.setUsuario(usuario);

        return repository.save(mensaje).getDataType();
    }

    public boolean bajaMensaje(int id, int idForo) {
        var foro = frepository.findBy_id(idForo);
        var mensaje = repository.findBy_id(id);
        foro.removeMensaje(mensaje);
        repository.delete(mensaje);
        return true;
    }

	public Object mensajesDeForo(int foroId, String token) {
		List<DtMensaje> retorno = new ArrayList<>();
        List<Mensaje> mensajes = repository.findAll();
        for(Mensaje m : mensajes){
            if(m.getForo().get_id() == foroId){
                retorno.add(m.getDataType());
            }
        }
        return retorno;
	}
}
