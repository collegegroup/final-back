package com.proyecto.api.controller;

import com.proyecto.api.model.Foro;
import com.proyecto.api.service.ForoService;
import com.proyecto.api.util.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/foros")
public class ForoController {

  @Autowired
  private ForoService service;

  // Testing
  @RequestMapping(value = "/", method = RequestMethod.GET)
  public ResponseEntity<Object> getAllForos() {
    new Log("[Inicio] Obtener Foros",Thread.currentThread()); //Log
    ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getAllForos(), HttpStatus.OK);
    new Log("[Fin] Obtener Foros",Thread.currentThread()); //Log
    return toReturn;
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public ResponseEntity<Object> getForoById(@PathVariable("id") Integer id) {
    new Log("[Inicio] Obtener Foro por ID",Thread.currentThread()); //Log
    ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getForoById(id), HttpStatus.OK);
    new Log("[Fin] Obtener Foro por ID",Thread.currentThread()); //Log
    return toReturn;
  }

  // ENDPOINTS APP

  @RequestMapping(value = "/byCurso/{cursoId}", method = RequestMethod.GET)
  public ResponseEntity<Object> getForosDeCurso(@PathVariable("cursoId") Integer cursoId) {
    new Log("[Inicio] Obtener Foros por Curso",Thread.currentThread()); //Log
    ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getForosDeCurso(cursoId), HttpStatus.OK);
    new Log("[Fin] Obtener Foros por Curso",Thread.currentThread()); //Log
    return toReturn;
  }

  @RequestMapping(value = "/byUsuario/{usuarioMail}", method = RequestMethod.GET)
  public ResponseEntity<Object> getForosDeUsuario(@PathVariable("usuarioMail") String usuarioMail) {
    new Log("[Inicio] Obtener Foros por Usuario",Thread.currentThread()); //Log
    ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getForosDeUsuario(usuarioMail), HttpStatus.OK);
    new Log("[Fin] Obtener Foros por Usuario",Thread.currentThread()); //Log
    return toReturn;
  }

  @RequestMapping(value = "/altaForo/{id}", method = RequestMethod.POST)
  public ResponseEntity<Object> altaForo(@PathVariable("id") Integer id, @Validated @RequestBody Foro foro) {
    new Log("[Inicio] Alta Foro",Thread.currentThread()); //Log
    ResponseEntity<Object> toReturn = new ResponseEntity<>(service.altaForo(foro, id), HttpStatus.OK);
    new Log("[Fin] Alta Foro",Thread.currentThread()); //Log
    return toReturn;
  }

  @RequestMapping(value = "/bajaForo/{id}/{idCurso}", method = RequestMethod.DELETE)
  public ResponseEntity<Object> bajaForo(@PathVariable("id") Integer id, @PathVariable("idCurso") Integer idCurso) {
    new Log("[Inicio] Baja Foro",Thread.currentThread()); //Log
    ResponseEntity<Object> toReturn = new ResponseEntity<>(service.bajaForo(id, idCurso), HttpStatus.OK);
    new Log("[Fin] Baja Foro",Thread.currentThread()); //Log
    return toReturn;
  }

  @RequestMapping(value = "/editarForo/{id}", method = RequestMethod.PUT)
  public ResponseEntity<Object> editarForo(@PathVariable("id") Integer id, @Validated @RequestBody Foro foro) {
    new Log("[Inicio] Editar Foro",Thread.currentThread()); //Log
    ResponseEntity<Object> toReturn = new ResponseEntity<>(service.editarForo(id, foro), HttpStatus.OK);
    new Log("[Fin] Editar Foro",Thread.currentThread()); //Log
    return toReturn;
  }
}
