package com.proyecto.api.dataTypes;

public class DtUsuario {
    
    public int _id;
    private char tipoUsu;
	private String nombre;
    private String mail;
	private String imagen;
    private String direccion;
    private String descripcion;
    private String tipoDocumento;
    private String documento;
    private String carrera;

    private String error="NONE";

    public DtUsuario(){}
    
    public DtUsuario(int _id, char tipoUsu, String nombre, String mail, String imagen, String direccion,
            String descripcion, String links, String tipoDocumento, String documento) {
        this._id = _id;
        this.tipoUsu = tipoUsu;
        this.nombre = nombre;
        this.mail = mail;
        this.imagen = imagen;
        this.direccion = direccion;
        this.descripcion = descripcion;
        this.tipoDocumento = tipoDocumento;
        this.documento = documento;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public char getTipoUsu() {
        return tipoUsu;
    }

    public void setTipoUsu(char tipoUsu) {
        this.tipoUsu = tipoUsu;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
    

}
