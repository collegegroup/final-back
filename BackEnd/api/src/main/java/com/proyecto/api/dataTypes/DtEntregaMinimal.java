package com.proyecto.api.dataTypes;

public class DtEntregaMinimal {
    
    private int id;
    private int idTarea;
    private String mailUsuario;
    private String linkEntrega;
    private double nota;
    private String error = "NONE";

    public DtEntregaMinimal(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMailUsuario() {
        return mailUsuario;
    }

    public void setMailUsuario(String mailUsuario) {
        this.mailUsuario = mailUsuario;
    }

    public String getLinkEntrega() {
        return linkEntrega;
    }

    public void setLinkEntrega(String linkEntrega) {
        this.linkEntrega = linkEntrega;
    }

    public double getNota() {
        return nota;
    }

    public void setNota(double nota) {
        this.nota = nota;
    }

    public int getIdTarea() {
        return idTarea;
    }

    public void setIdTarea(int idTarea) {
        this.idTarea = idTarea;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    

}
