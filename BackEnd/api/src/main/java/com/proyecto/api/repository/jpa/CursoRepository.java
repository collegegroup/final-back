package com.proyecto.api.repository.jpa;

import com.proyecto.api.model.Curso;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface CursoRepository extends JpaRepository<Curso,String> {
    Curso findBy_id(int _id);
    Curso findByNombre(String nombre);

    @Query(
        value = "SELECT * FROM cursos AS cu INNER JOIN cursos_libros AS cl ON cu._id = cl.cursos__id WHERE cl.libros__id = :id", 
        nativeQuery = true)
    Curso getCursoDeLibro(@Param("id") int _id);   
    
    
    @Query(
        value = "SELECT * FROM cursos AS cu INNER JOIN cursos_tareas AS ct ON cu._id = ct.cursos__id WHERE ct.tareas__id = :id", 
        nativeQuery = true)
    Curso getCursoDeTarea(@Param("id") int _id);   

}
