package com.proyecto.api.service;

import java.util.ArrayList;
import java.util.List;

import com.proyecto.api.dataTypes.DtTarea;
import com.proyecto.api.dataTypes.DtUsuario;
import com.proyecto.api.model.Curso;
import com.proyecto.api.model.Docente;
import com.proyecto.api.model.DocenteCurso;
import com.proyecto.api.model.Email;
import com.proyecto.api.model.Entrega;
import com.proyecto.api.model.Estudiante;
import com.proyecto.api.model.Inscripcion;
import com.proyecto.api.model.Tarea;
import com.proyecto.api.model.Usuario;
import com.proyecto.api.repository.jpa.CursoRepository;
import com.proyecto.api.repository.jpa.EntregaRepository;
import com.proyecto.api.repository.jpa.TareaRepository;
import com.proyecto.api.repository.jpa.UsuarioRepository;
import com.proyecto.api.util.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TareaService {

    @Autowired
    private TareaRepository repository;
    @Autowired
    private CursoRepository Crepository;
    @Autowired
    private UsuarioRepository urepository;
    @Autowired
    private EntregaRepository entregaRepository;

    public List<DtTarea> getAllTareas() {
        List<DtTarea> retorno = new ArrayList<>();
        List<Tarea> tareas = repository.findAll();
        for (Tarea t : tareas) {
            retorno.add(t.getDataType());
        }
        return retorno;
    }

    public DtTarea getTareaById(int id) {
        return repository.findBy_id(id).getDataType();
    }

    public List<DtTarea> getTareaDeCurso(Integer cursoId){
        List<DtTarea> retorno = new ArrayList<>();
        Curso curso = Crepository.findBy_id(cursoId);
        try {
            for (Tarea t : curso.getTareas()) {
                DtTarea aux = t.getDataType();
                retorno.add(aux);
            }
            return retorno;
        } catch (Exception e) {
            //devuelvo array vacio
            System.out.println("Error: " + e);
            return new ArrayList<DtTarea>();
        }
    }

    public List<DtTarea> getTareaDeUsuario(String mailUsuario){
        List<DtTarea> retorno = new ArrayList<>();
        Usuario user = urepository.findByMail(mailUsuario);
        try {
            //si falla la funcion, ver el casteo este
            DtUsuario dtuser = user.getDataTypeBase();
            
            if(dtuser.getTipoUsu() == 'E'){
                Estudiante student = (Estudiante) user;
                for (Inscripcion e : student.getInscripcionCurso()) {
                    for(Tarea t : e.getCurso().getTareas()){
                        DtTarea aux = t.getDataType();
                        retorno.add(aux);
                    }
                }
            }else if(dtuser.getTipoUsu() == 'D'){
                Docente docente = (Docente) user;
                for(DocenteCurso dc : docente.getDocenteCursos()){
                    for(Tarea t : dc.getCurso().getTareas()){
                        DtTarea aux = t.getDataType();
                        retorno.add(aux);
                    }
                }
            }else{
                DtTarea aux = new DtTarea();
                aux.setError("El usuario no es ni Estudiante ni Docente");
                retorno.add(aux);
            }

            return retorno;
        } catch (Exception e) {
            //devuelvo array vacio si hay exception
            System.out.println("Error: " + e);
            return new ArrayList<DtTarea>();
        }
    }

    public DtTarea altaTarea(Tarea tarea, Integer id) {
        var curso = Crepository.findBy_id(id);
        tarea.setAutoEntregaCalculada(false);
        curso.getTareas().add(tarea);
        List<Inscripcion> usuarios = curso.getInscriptos();
        List<String> tokens = new ArrayList<>();
        String mails = "";
        for (Inscripcion usuario : usuarios) {
            tokens.add(usuario.getEstudiante().getToken());
            mails+=usuario.getEstudiante().getMail() + ",";
        }
        Email e = new Email();
        e.setTo(mails);
        e.setFrom("noreplysapio@gmail.com");
        e.setPassword("Proyecto2020.");
        e.setSubject("Nueva tarea - Sapp.io");
        e.setBody("<html>" + "<header>"
                + "<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' integrity='sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin='anonymous'>"
                + "</header>" + "<body>" + "<h4>Hola, parece que tienes una nueva tarea</h4>"
                + "<p>Hay una tarea nueva en el curso "+curso.getNombre()+".</p>"
                + "<p>Saludos</p>" + "<p>Equipo de Sapp.io</p>" + "</body>" + "</html>");

        EmailService.sendEmail(e);
        String title = "Nueva Tarea - Sapp.io";
        String body = "Hay una nueva tarea en el curso " + curso.getNombre();
        NotificationsService.SendNotifications(title, body, tokens);
        return repository.save(tarea).getDataType();
    }

    public boolean bajaTarea(int id, int idcurso) {
        try {
            var curso = Crepository.findBy_id(idcurso);
            var tarea = repository.findBy_id(id);
            
            curso.getTareas().remove(tarea);
            Crepository.save(curso);

            List<Entrega> entregas = tarea.getEntregas();
            for(Entrega e : entregas){
                entregaRepository.delete(e);
            }

            repository.delete(tarea);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public DtTarea editarTarea(int id, Tarea tarea) {
        var t = repository.findBy_id(id);
        t.setDescripcion(tarea.getDescripcion());
        t.setTitulo(tarea.getTitulo());
        t.setFechaLimite(tarea.getFechaLimite());
        t.setEntregable(tarea.isEntregable());
        return repository.save(t).getDataType();
    }

	public void bTarea(Tarea t) {
	}
}
