package com.proyecto.api.model;

import java.io.Serializable;

import com.proyecto.api.dataTypes.DtUsuario;

public class JwtResponse implements Serializable {

	private static final long serialVersionUID = -8091879091924046844L;
	private final String jwttoken;
	private DtUsuario usuario;

	public JwtResponse(String jwttoken,DtUsuario usuario) {
		this.jwttoken = jwttoken;
		this.usuario = usuario;
	}

	public String getToken() {
		return this.jwttoken;
	}

	public DtUsuario getUsuario() {
		return this.usuario;
	}
}