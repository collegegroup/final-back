package com.proyecto.api.repository.jpa;

import java.util.List;

import com.proyecto.api.model.Entrega;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface EntregaRepository extends JpaRepository<Entrega,String> {

    Entrega findBy_id(int _id);

    @Query(
        value = "SELECT * FROM entregas AS m WHERE m._estudianteid = :estudianteId", 
        nativeQuery = true)
    List<Entrega> getEntregasDeEstudiante(@Param("estudianteId") int estudianteId);

}
