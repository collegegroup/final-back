package com.proyecto.api.controller;

import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.proyecto.api.model.Curso;
import com.proyecto.api.service.CursoService;
import com.proyecto.api.util.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/cursos")
public class CursoController {

    @Autowired
    private CursoService service;

    // Testing

    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<Object> getAllCursos1() {
        new Log("[Inicio] Obtener cursos",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getAllCursos(), HttpStatus.OK);
        new Log("[Fin] Obtener cursos",Thread.currentThread()); //Log
        return toReturn;
    }
    
    @RequestMapping(value = "/obtenerCursos", method = RequestMethod.GET)
    public ResponseEntity<Object> getAllCursos2() {
        new Log("[Inicio] Obtener cursos",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getAllCursos(), HttpStatus.OK);
        new Log("[Fin] Obtener cursos",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getCursoById(@PathVariable("id") int id) {
        new Log("[Inicio] Obtener curso por ID",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getCursoById(id), HttpStatus.OK);
        new Log("[Fin] Obtener curso por ID",Thread.currentThread()); //Log
        return toReturn;
    }

    // ENDPOINTS APP

    @RequestMapping(value = "/altaCurso", method = RequestMethod.POST)
    public ResponseEntity<Object> altaCurso(Integer id, @Validated @RequestBody Curso curso, @RequestHeader Map<String,String> headers) {
        new Log("[Inicio] Alta Curso",Thread.currentThread()); //Log
        String token = headers.get("authorization").substring(7);
        if(curso.getColor().equals("")){
            curso.setColor("0xFF005377");
        }
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.altaCurso(curso, token), HttpStatus.OK);
        new Log("[Fin] Alta Curso",Thread.currentThread()); //Log
        return toReturn;
    }

    //CARGA MASIVA
    @RequestMapping(value = "/altaCurso/csv", method = RequestMethod.POST)
    public ResponseEntity<Object> altaMasivaCursos(@RequestBody String csv, @RequestHeader Map<String,String> headers){
        new Log("[Inicio] Alta Curso Masivo",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = null;
        String token = headers.get("authorization").substring(7);

        toReturn = new ResponseEntity<>(service.saveCursos(csv, token), HttpStatus.OK);

        new Log("[Fin] Alta Curso Masivo",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/bajaCurso/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> bajaCurso(@PathVariable("id") int id, @RequestHeader Map<String,String> headers) {
        new Log("[Inicio] Baja Curso",Thread.currentThread()); //Log
        String token = headers.get("authorization").substring(7);
        ResponseEntity<Object> toReturn =  new ResponseEntity<>(service.bajaCurso(id, token), HttpStatus.OK);
        new Log("[Fin] Baja Curso",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/editarCurso/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> editarCurso(@PathVariable("id") int id, @Validated @RequestBody ObjectNode jsonData, @RequestHeader Map<String,String> headers) {
        new Log("[Inicio] Editar Curso",Thread.currentThread()); //Log
        Curso curso = new Curso();
        String token = headers.get("authorization").substring(7);
        try {
            curso = new ObjectMapper().readValue(jsonData.get("Curso").toString(), Curso.class);
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.editarCurso(id, curso, token), HttpStatus.OK);
        new Log("[Fin] Editar Curso",Thread.currentThread()); //Log

        return toReturn;
    }

    @RequestMapping(value = "/obtenerCursosByUsuario/{mail}", method = RequestMethod.GET)
    public ResponseEntity<Object> getCursosByUsuario(@PathVariable("mail") String mail, @RequestHeader Map<String,String> headers) {
        new Log("[Inicio] Obtener Cursos de Usuario",Thread.currentThread()); //Log
        String token = headers.get("authorization").substring(7);
        ResponseEntity<Object> toReturn =  new ResponseEntity<>(service.getCursosByUsuario(mail,token), HttpStatus.OK);
        new Log("[Fin] Obtener Cursos de Usuario",Thread.currentThread()); //Log
        return toReturn;
    }

}
