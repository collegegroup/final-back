package com.proyecto.api.service;

import java.util.ArrayList;
import java.util.List;

import com.proyecto.api.model.Contenido;
import com.proyecto.api.model.Curso;
import com.proyecto.api.model.Libro;
import com.proyecto.api.repository.jpa.ContenidoRepository;
import com.proyecto.api.repository.jpa.CursoRepository;
import com.proyecto.api.repository.jpa.LibroRepository;
import com.proyecto.api.dataTypes.DtContenido;
import com.proyecto.api.dataTypes.DtUsuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContenidoService {

    @Autowired
    private LibroRepository libroRepository;
    @Autowired
    private ContenidoRepository repository;
    @Autowired
    private CursoRepository cursoRepository;
    @Autowired
    private UsuarioService userService;
    @Autowired
    private CursoService cursoService;


    public List<Contenido> getAllContenidos() {
        return (List<Contenido>) repository.findAll();
    }

    public Contenido getContenidoById(int id) {
        return repository.findBy_id(id);
    }

    public List<DtContenido> getContenidoByLibroId(int id, String token) {
        DtUsuario requestedBy = userService.getUserByToken(token);
        List<DtContenido> toReturn = new ArrayList<DtContenido>();

        if(requestedBy.getTipoUsu() == 'A'){
            Libro l = libroRepository.findBy_id(id);
            List<Contenido> lisCont = l.getContenidos();

            for(Contenido cont : lisCont){
                toReturn.add(cont.getDataType());
            }
        }else{
            Curso deLibro = cursoRepository.getCursoDeLibro(id);
            if(cursoService.estaUsuarioEnCurso(requestedBy.getMail(), deLibro)){
                Libro l = libroRepository.findBy_id(id);
                List<Contenido> lisCont = l.getContenidos();

                for(Contenido cont : lisCont){
                    toReturn.add(cont.getDataType());
                }
            }else{
                DtContenido temp = new DtContenido();
                if(requestedBy.getTipoUsu() == 'E'){
                    temp.setError("No esta Inscripto en este Curso");
                }else{
                    temp.setError("No fue asignado como Docente de este Curso");
                }
                toReturn.add(temp);
            }
        }

        return toReturn;
    }

    

    public Contenido altaContenido(Contenido contenido, Integer id) {
        var libro = libroRepository.findBy_id(id);      

        
        libro.getContenidos().add(contenido);
        repository.save(contenido);
        return contenido;
    }

    public boolean bajaContenido(int id, int idLibro) {
        try {
            var libro = libroRepository.findBy_id(idLibro);
            var contenido = repository.findBy_id(id);
            libro.getContenidos().remove(contenido);
            repository.delete(repository.findBy_id(id));
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public Contenido editarContenido(int id, Contenido contenido) {
        var c = repository.findBy_id(id);        

        c.setContenido(contenido.getContenido());
        c.setTipo(contenido.getTipo());
        repository.save(c);
        return c;
    }

}
