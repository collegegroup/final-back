package com.proyecto.api.service;

import com.proyecto.api.dataTypes.DtInscripcion;
import com.proyecto.api.dataTypes.DtNotaFinal;
import com.proyecto.api.dataTypes.DtUsuario;
import com.proyecto.api.model.Curso;
import com.proyecto.api.model.Docente;
import com.proyecto.api.model.DocenteCurso;
import com.proyecto.api.model.Email;
import com.proyecto.api.model.Estudiante;
import com.proyecto.api.model.Inscripcion;
import com.proyecto.api.model.Usuario;
import com.proyecto.api.repository.jpa.CursoRepository;
import com.proyecto.api.repository.jpa.DocenteCursoRepository;
import com.proyecto.api.repository.jpa.InscripcionRepository;
import com.proyecto.api.repository.jpa.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class InscripcionService {

    @Autowired
    private InscripcionRepository repository;

    @Autowired
    private DocenteCursoRepository dCRepository;

    @Autowired
    private UsuarioRepository uRepository;

    @Autowired
    private CursoRepository cRepository;

    @Autowired
	private UsuarioService service;

    public List<Inscripcion> getAllInscripciones() {
        return (List<Inscripcion>) repository.findAll();
    }

    public Inscripcion getInscripcionById(int id) {
        return repository.findBy_id(id);
    }

    public DtInscripcion saveInscripcion(String[] datos, int cursoId, String usuarioEmail, String token) {

        DtInscripcion toReturn = null;
        DtUsuario usu = null;

        try{
            usu = uRepository.findByMail(usuarioEmail).getDataTypeBase();
        
            DtUsuario requestedBy = service.getUserByToken(token);

            if(requestedBy.getTipoUsu() == 'A'){
                if(usu.getTipoUsu() == 'E'){

                    Inscripcion inscripcion = new Inscripcion();

                    inscripcion.setNotaFinal(Double.parseDouble(datos[0]));
                    String temp;

                    try{
                        temp = datos[1].substring(1, datos[1].length()-1);
                        inscripcion.setFechaInicial(new SimpleDateFormat("yyyy-MM-dd").parse(temp));
                    }catch(Exception e){
                        inscripcion.setFechaInicial(null);
                    }
                    try{
                        temp = datos[2].substring(1, datos[2].length()-1);
                        inscripcion.setFechaFinal(new SimpleDateFormat("yyyy-MM-dd").parse(temp));
                    }catch(Exception e){
                        inscripcion.setFechaFinal(null);
                    }

                    Estudiante user = (Estudiante) uRepository.findByMail(usuarioEmail);
                    user.getInscripcionCurso().add(inscripcion);
                    inscripcion.setEstudiante(user);
            
                    Curso curso = cRepository.findBy_id(cursoId);
                    curso.getInscriptos().add(inscripcion);
                    inscripcion.setCurso(curso);
            
                    repository.save(inscripcion);
                    toReturn = inscripcion.getDataType();

                }else if(usu.getTipoUsu() == 'D'){

                    DocenteCurso inscrip = new DocenteCurso();

                    Date t = new Date();

                    int hora = Integer.parseInt(datos[3].split(":")[0]);
                    int minuto = Integer.parseInt(datos[3].split(":")[1]);
                    int segundo = Integer.parseInt(datos[3].split(":")[2]);

                    t.setHours(hora);
                    t.setMinutes(minuto);
                    t.setSeconds(segundo);
                    inscrip.setHorario(t);
                    
                    Docente user = (Docente) uRepository.findByMail(usuarioEmail);
                    user.getDocenteCursos().add(inscrip);
                    inscrip.setDocente(user);

                    Curso curso = cRepository.findBy_id(cursoId);
                    curso.getDocentesAsignados().add(inscrip);
                    inscrip.setCurso(curso);

                    dCRepository.save(inscrip);
                    toReturn = inscrip.getDataType();

                }else{
                    toReturn = new DtInscripcion();
                    toReturn.setError("Eres Administrador, no puedes registrarte a un curso");
                }
            }else{
                toReturn = new DtInscripcion();
                toReturn.setError("No eres Administrador");
            }
        }catch(Exception e){
            toReturn = new DtInscripcion();
            toReturn.setError("El usuario "+usuarioEmail+" no existe"+e.getMessage()+e.getStackTrace().toString());
        }
        return toReturn;
    }

    public DtInscripcion saveInscripcionDocente(DocenteCurso inscripcion, int cursoId, String docenteEmail, String token) {
        Docente user = (Docente) uRepository.findByMail(docenteEmail);
        user.getDocenteCursos().add(inscripcion);
        inscripcion.setDocente(user);

        Curso curso = cRepository.findBy_id(cursoId);
        curso.getDocentesAsignados().add(inscripcion);
        inscripcion.setCurso(curso);

        dCRepository.save(inscripcion);
        return inscripcion.getDataType();
    }

    public DtInscripcion editarInscripcion(int inscripcionId, Inscripcion inscripcion) {
        Inscripcion inscriOld = repository.findBy_id(inscripcionId);
        inscriOld.setFechaFinal(inscripcion.getFechaFinal());
        inscriOld.setFechaInicial(inscripcion.getFechaInicial());
        repository.save(inscriOld);
        return inscriOld.getDataType();
    }

    public DtInscripcion editarInscripcion(int inscripcionId, DocenteCurso inscripcion) {
        DocenteCurso inscriOld = dCRepository.findBy_id(inscripcionId);
        inscriOld.setHorario(inscripcion.getHorario());
        dCRepository.save(inscriOld);
        return inscriOld.getDataType();
    }

    public boolean bajaInscripcionEstudiante(int id) {
        try {
            Inscripcion inscri = repository.findBy_id(id);

            var curso = cRepository.findBy_id(inscri.getCurso().get_id());
            Estudiante estudiante = (Estudiante) uRepository.findBy_id(inscri.getEstudiante().get_id());

            curso.getInscriptos().remove(inscri);
            estudiante.getInscripcionCurso().remove(inscri);

            repository.delete(inscri);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean bajaInscripcionDocente(int id) {
        try {
            DocenteCurso inscri = dCRepository.findBy_id(id);

            var curso = cRepository.findBy_id(inscri.getCurso().get_id());
            Docente docente = (Docente) uRepository.findBy_id(inscri.getDocente().get_id());

            curso.getDocentesAsignados().remove(inscri);
            docente.getDocenteCursos().remove(inscri);

            dCRepository.delete(inscri);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public DtInscripcion altaCalificacionFinal(int id, Double calificacion, String mail) {
        Inscripcion inscri = null;
        List<Inscripcion> inscripciones = repository.findAll();

        int i = 0;
        boolean encontrado = false;
        while(i < inscripciones.size() && !encontrado){
            if(inscripciones.get(i).getCurso().get_id() == id && inscripciones.get(i).getEstudiante().getMail().equals(mail)){
                inscri = inscripciones.get(i);
                encontrado = true;
            }
            i++;
        }

        if(inscri != null){
            inscri.setNotaFinal(calificacion);
            repository.save(inscri);
            List<String> tokens = new ArrayList<>();
            String email = "";
            tokens.add(inscri.getEstudiante().getToken());
            email = inscri.getEstudiante().getMail();
            Email e = new Email();
            e.setTo(email);
            e.setFrom("noreplysapio@gmail.com");
            e.setPassword("Proyecto2020.");
            e.setSubject("Calificación Final - Sapp.io");
            e.setBody("<html>" + "<header>"
                    + "<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' integrity='sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin='anonymous'>"
                    + "</header>" + "<body>" + "<h4>Hola, fuiste calificado.</h4>"
                    + "<p>Tu calificación final en el curso "+inscri.getCurso().getNombre()+" es "+calificacion+".</p>"
                    + "<p>Saludos</p>" + "<p>Equipo de Sapp.io</p>" + "</body>" + "</html>");

            EmailService.sendEmail(e);
            String title = "Calificación Final - Sapp.io";
            String body = "Tu calificación final en el curso " + inscri.getCurso().getNombre() + " es " + calificacion;
            NotificationsService.SendNotifications(title, body, tokens);
            return inscri.getDataType();
        }else{
            DtInscripcion toReturn = new DtInscripcion();
            toReturn.setError("No se encontro la inscripcion de ese usuario en ese curso.");
            return toReturn;
        }


    }

    //CARGA MASIVA INSCRIPCION
    public List<Inscripcion> cargaMasivaInscripcion(Integer cursoId, List<String> userMails) {
        List<Inscripcion> devuelvo = new ArrayList<Inscripcion>();
        for (String email : userMails){

            Inscripcion aux = new Inscripcion();
            Curso curso = cRepository.findById(cursoId.toString()).get();

            aux.setCurso(cRepository.findBy_id(cursoId));
            aux.setEstudiante((Estudiante)uRepository.findByMail(email));
            aux.setFechaInicial(curso.getFechaInicio());

            String fechaFindeAno = LocalDate.now().with(TemporalAdjusters.lastDayOfYear()).toString();
            Date fechaFinal = new Date(Date.parse(fechaFindeAno));
            aux.setFechaFinal(fechaFinal);

            devuelvo.add(aux);
        }
        repository.saveAll(devuelvo);
        return devuelvo;
    }

    //CARGA MASIVA DOCENTECURSO
    public List<DocenteCurso> cargaMasivaDocenteCurso(Integer cursoId, List<String> userMails) {
        List<DocenteCurso> devuelvo = new ArrayList<DocenteCurso>();
        for (String email : userMails){

            DocenteCurso aux = new DocenteCurso();
            Curso curso = cRepository.findById(cursoId.toString()).get();

            aux.setCurso(cRepository.findBy_id(cursoId));
            aux.setDocente((Docente)uRepository.findByMail(email));
            //de momento lo dejamos asi
            aux.setHorario(curso.getFechaInicio());

            devuelvo.add(aux);
        }
        dCRepository.saveAll(devuelvo);
        return devuelvo;
    }

	public Object bajaInscripcion(int cursoId, String usuarioEmail, String token) {
        DtUsuario usu = null;
        Usuario usuFull = null;
        DtInscripcion toReturn = new DtInscripcion();

        try{
            usuFull = uRepository.findByMail(usuarioEmail);
            usu = usuFull.getDataTypeBase();
        
            DtUsuario requestedBy = service.getUserByToken(token);

            if(requestedBy.getTipoUsu() == 'A'){
                var curso = cRepository.findBy_id(cursoId);

                if(usu.getTipoUsu() == 'E'){

                    Estudiante estudiante = (Estudiante) usuFull;
                    Inscripcion esta = null;

                    List<Inscripcion> li = curso.getInscriptos();
                    int i = 0;
                    while(i < li.size() && esta == null){
                        Inscripcion in = li.get(i);
                        if(in.getEstudiante().getMail().equals(estudiante.getMail())){
                            esta = in;
                        }
                        i++;
                    }

                    curso.getInscriptos().remove(esta);
                    estudiante.getInscripcionCurso().remove(esta);
        
                    repository.delete(esta);

                }else if(usu.getTipoUsu() == 'D'){
                    Docente docente = (Docente) usuFull;
                    DocenteCurso esta = null;

                    List<DocenteCurso> li = curso.getDocentesAsignados();
                    int i = 0;
                    while(i < li.size() && esta == null){
                        DocenteCurso in = li.get(i);
                        if(in.getDocente().getMail().equals(docente.getMail())){
                            esta = in;
                        }
                        i++;
                    }

                    curso.getDocentesAsignados().remove(esta);
                    docente.getDocenteCursos().remove(esta);
        
                    dCRepository.delete(esta);
                }

            }else{
                toReturn.setError("No eres Administrador");
            }

        }catch(Exception e){
            toReturn.setError("No existe el usuario");
        }
        
        return toReturn;
	}

	public double getCalificacionFinal(int id, String mail) {
        double toReturn = -1;
        
        List<Inscripcion> inscripciones = repository.findAll();

        int i = 0;
        boolean encontrado = false;
        while(i < inscripciones.size() && !encontrado){
            if(inscripciones.get(i).getCurso().get_id() == id && inscripciones.get(i).getEstudiante().getMail().equals(mail)){
                toReturn = inscripciones.get(i).getNotaFinal();
                encontrado = true;
            }
            i++;
        }

        return toReturn;
	}

	public List<DtNotaFinal> getCalificacionesCurso(int idCurso) {
		List<DtNotaFinal> toReturn = new ArrayList<DtNotaFinal>();
        
        List<Inscripcion> inscripciones = repository.getInscripcionesDeCurso(idCurso);

        int i = 0;
        while(i < inscripciones.size()){
            toReturn.add(inscripciones.get(i).getDtNotaFinal());
            i++;
        }

        return toReturn;
	}
}
