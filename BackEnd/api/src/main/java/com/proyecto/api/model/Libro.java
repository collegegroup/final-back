package com.proyecto.api.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.proyecto.api.dataTypes.DtLibro;

@Entity
@Table(name = "libros", schema = "public")
public class Libro {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int _id;

    @Column(name = "titulo")
    private String titulo;

    @Column(name = "color")
    private String color;

    @OneToMany(orphanRemoval = true)
    private List<Contenido> contenidos;

    public Libro(){}
    public Libro(int _id, String titulo, String color, List<Contenido> contenidos) {
        this._id = _id;
        this.titulo = titulo;
        this.color = color;
        this.contenidos = contenidos;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<Contenido> getContenidos() {
        return contenidos;
    }

    public void setContenidos(List<Contenido> contenidos) {
        this.contenidos = contenidos;
    }

    public DtLibro getDataType(){
        DtLibro toReturn = new DtLibro();

        toReturn.setColor(color);
        toReturn.setTitulo(titulo);
        toReturn.set_id(_id);

        return toReturn;
    }

}
