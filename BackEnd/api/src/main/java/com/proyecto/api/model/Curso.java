package com.proyecto.api.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.proyecto.api.dataTypes.DtCurso;

@Entity
@Table(name = "cursos", schema = "public")
public class Curso {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int _id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "creditos")
    private Integer creditos;

    @Column(name = "fechaInicio")
    private Date fechaInicio;

    @Column(name = "color")
    private String color;

    //////////////////////////////////////////////////// Relaciones
    //////////////////////////////////////////////////// ///////////////////////////////////

    @OneToMany(mappedBy = "curso")
    private List<Inscripcion> inscriptos;

    @OneToMany(mappedBy = "curso")
    private List<DocenteCurso> docentesAsignados;

    @OneToMany(orphanRemoval = true)
    private List<Foro> foros;

    @OneToMany(orphanRemoval = true)
    private List<Tarea> tareas;

    @OneToMany(orphanRemoval = true)
    private List<Libro> libros;

    //////////////////////////////////////////////////// Fin de Bloque
    //////////////////////////////////////////////////// ////////////////////////////////

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getCreditos() {
        return creditos;
    }

    public void setCreditos(Integer creditos) {
        this.creditos = creditos;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public List<Inscripcion> getInscriptos() {
        return inscriptos;
    }

    public void setInscriptos(List<Inscripcion> inscriptos) {
        this.inscriptos = inscriptos;
    }

    public List<DocenteCurso> getDocentesAsignados() {
        return docentesAsignados;
    }

    public void setDocentesAsignados(List<DocenteCurso> docentesAsignados) {
        this.docentesAsignados = docentesAsignados;
    }

    public List<Foro> getForos() {
        return foros;
    }

    public void setForos(List<Foro> foros) {
        this.foros = foros;
    }

    public List<Tarea> getTareas() {
        return tareas;
    }

    public void setTareas(List<Tarea> tareas) {
        this.tareas = tareas;
    }

    public List<Libro> getLibros() {
        return libros;
    }

    public void setLibros(List<Libro> libros) {
        this.libros = libros;
    }
    
    public Curso() {
    }

    public Curso(int _id, String nombre, String descripcion, Integer creditos,  Date fechaInicio, String color,
            List<Inscripcion> inscriptos, List<DocenteCurso> docentesAsignados, List<Foro> foros, List<Tarea> tareas,
            List<Libro> libros) {
        this._id = _id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.creditos = creditos;
        this.fechaInicio = fechaInicio;
        this.inscriptos = inscriptos;
        this.docentesAsignados = docentesAsignados;
        this.foros = foros;
        this.tareas = tareas;
        this.libros = libros;
        this.color = color;
    }

    public DtCurso getDataType(){
        DtCurso toReturn = new DtCurso();

        toReturn.setCreditos(creditos);
        toReturn.setDescripcion(descripcion);
        toReturn.setFechaInicio(fechaInicio);
        toReturn.setNombre(nombre);
        toReturn.set_id(_id);
        toReturn.setColor(color);

        return toReturn;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

}
