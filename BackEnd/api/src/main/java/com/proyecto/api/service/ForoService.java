package com.proyecto.api.service;

import java.util.ArrayList;
import java.util.List;

import com.proyecto.api.dataTypes.DtForo;
import com.proyecto.api.model.Curso;
import com.proyecto.api.model.Docente;
import com.proyecto.api.model.DocenteCurso;
import com.proyecto.api.model.Email;
import com.proyecto.api.model.Estudiante;
import com.proyecto.api.model.Foro;
import com.proyecto.api.model.Inscripcion;
import com.proyecto.api.model.Mensaje;
import com.proyecto.api.model.Usuario;
import com.proyecto.api.repository.jpa.CursoRepository;
import com.proyecto.api.repository.jpa.ForoRepository;
import com.proyecto.api.repository.jpa.MensajeRepository;
import com.proyecto.api.repository.jpa.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ForoService {

    @Autowired
    private ForoRepository repository;
    @Autowired
    private CursoRepository Crepository;
    @Autowired
    private UsuarioRepository urepository;
    @Autowired
    private MensajeRepository mensajeRepository;

    public List<DtForo> getAllForos() {
        List<DtForo> retorno = new ArrayList<>();
        List<Foro> foros = repository.findAll();
        for(Foro f : foros){
            retorno.add(f.getDataType());
        }
        return retorno;
    }

    public DtForo getForoById(int id) {
        return repository.findBy_id(id).getDataType();
    }

    public List<DtForo> getForosDeCurso(Integer cursoId) {
        List<DtForo> retorno = new ArrayList<DtForo>();
        Curso curso = Crepository.findBy_id(cursoId);
        for (Foro f : curso.getForos()){
            retorno.add(f.getDataType());
        }
        return retorno;
    }

    public List<DtForo> getForosDeUsuario(String usuarioMail) {
        List<DtForo> retorno = new ArrayList<DtForo>();
        Usuario user = urepository.findByMail(usuarioMail);
        switch (user.getDataTypeBase().getTipoUsu()) {
            case 'E':
                Estudiante student = (Estudiante) user;
                for (Inscripcion i : student.getInscripcionCurso()){
                    List<Foro> foros = i.getCurso().getForos();
                    for (Foro f : foros) {
                        retorno.add(f.getDataType());
                    }
                }
                break;
            case 'A':
                //devuelvo array vacio
                System.out.println("Error: un administrador no tiene foros a que acceder.");
                break;
            case 'D':
                Docente teacher = (Docente) user;
                for (DocenteCurso dc : teacher.getDocenteCursos()){
                    List<Foro> foros = dc.getCurso().getForos();
                    for (Foro f : foros) {
                        retorno.add(f.getDataType());
                    }
                }
                break;
        }
        return retorno;
    }

    public DtForo altaForo(Foro foro, Integer id) {
        var curso = Crepository.findBy_id(id);
        curso.getForos().add(foro);
        List<Inscripcion> usuarios = curso.getInscriptos();
        List<String> tokens = new ArrayList<>();
        String mails = "";
        for (Inscripcion usuario : usuarios) {
            tokens.add(usuario.getEstudiante().getToken());
            mails += usuario.getEstudiante().getMail() + ","; 
        }
        Email e = new Email();
        e.setTo(mails);
        e.setFrom("noreplysapio@gmail.com");
        e.setPassword("Proyecto2020.");
        e.setSubject("Nuevo Foro - Sapp.io");
        e.setBody("<html>" + "<header>"
                + "<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' integrity='sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin='anonymous'>"
                + "</header>" + "<body>" + "<h4>Hola!</h4>"
                + "<p>Hay un nuevo foro llamado "+foro.getNombre()+" en el curso "+curso.getNombre()+".</p>"
                + "<p>Saludos</p>" + "<p>Equipo de Sapp.io</p>" + "</body>" + "</html>");

        EmailService.sendEmail(e);
        String title = "Nuevo Foro - Sapp.io";
        String body = "Hay un nuevo foro llamado "+foro.getNombre()+" en el curso " + curso.getNombre();
        NotificationsService.SendNotifications(title, body, tokens);
        return repository.save(foro).getDataType();
    }

    public boolean bajaForo(int id, int idcurso) {
        try {
            var curso = Crepository.findBy_id(idcurso);
            var foro = repository.findBy_id(id);
            curso.getForos().remove(foro);

            List<Mensaje> mensajes = foro.getMensajes();
            for(Mensaje m : mensajes){
                mensajeRepository.delete(m);
            }

            repository.delete(repository.findBy_id(id));
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public DtForo editarForo(int id, Foro foro) {
        var f = repository.findBy_id(id);
        f.setNombre(foro.getNombre());
        f.setTipo(foro.getTipo());
        f.setFecha(foro.getFecha());        
        return repository.save(f).getDataType();
    }

	public void bForo(Foro f) {
	}
}