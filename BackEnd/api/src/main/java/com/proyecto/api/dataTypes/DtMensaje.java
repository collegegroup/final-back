package com.proyecto.api.dataTypes;

public class DtMensaje {

    private int _id;
    private DtUsuario usuario;
    private int foro;
    private String titulo;
    private String contenido;

    public DtMensaje() {}
    
    public DtMensaje(int _id, DtUsuario usuario, int foro, String titulo, String contenido) {
        this._id = _id;
        this.usuario = usuario;
        this.foro = foro;
        this.titulo = titulo;
        this.contenido = contenido;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public DtUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(DtUsuario usuario) {
        this.usuario = usuario;
    }

    public int getForo() {
        return foro;
    }

    public void setForo(int foro) {
        this.foro = foro;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }
    
    

}