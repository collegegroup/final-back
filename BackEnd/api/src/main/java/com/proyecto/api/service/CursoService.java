package com.proyecto.api.service;

import java.util.ArrayList;
import java.util.List;

import com.proyecto.api.dataTypes.DtCurso;
import com.proyecto.api.dataTypes.DtCursoConNota;
import com.proyecto.api.dataTypes.DtUsuario;
import com.proyecto.api.model.Curso;
import com.proyecto.api.model.Docente;
import com.proyecto.api.model.DocenteCurso;
import com.proyecto.api.model.Estudiante;
import com.proyecto.api.model.Foro;
import com.proyecto.api.model.Inscripcion;
import com.proyecto.api.model.Libro;
import com.proyecto.api.model.Tarea;
import com.proyecto.api.model.Usuario;
import com.proyecto.api.repository.jpa.CursoRepository;
import com.proyecto.api.repository.jpa.DocenteCursoRepository;
import com.proyecto.api.repository.jpa.InscripcionRepository;
import com.proyecto.api.repository.jpa.UsuarioRepository;
import com.proyecto.api.util.CSVHelper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CursoService {

    @Autowired
    private InscripcionRepository inscRepository;
    @Autowired
    private DocenteCursoRepository docCurRepository;
    @Autowired
    private CursoRepository repository;
    @Autowired
    private UsuarioRepository userRepository;

    @Autowired
    private UsuarioService userService;
    @Autowired
    private LibroService libroService;
    @Autowired
    private TareaService tareaService;
    @Autowired
    private ForoService foroService;

    public List<DtCurso> getAllCursos() {
        List<DtCurso> retorno = new ArrayList<>();
        List<Curso> cursos = repository.findAll();
        for (Curso c : cursos) {
            retorno.add(c.getDataType());
        }
        return retorno;
    }

    public DtCurso getCursoById(int id) {
        return repository.findBy_id(id).getDataType();
    }

    public DtCurso altaCurso(Curso curso, String token) {
        DtUsuario requestedBy = userService.getUserByToken(token);

        if(requestedBy.getTipoUsu() == 'A'){
            repository.save(curso);
            return curso.getDataType();
        }else{
            DtCurso c = curso.getDataType();
            c.setError("No eres Administrador");
            return c;
        }

    }

    // CARGA MASIVA
    public List<DtCurso> saveCursos(String file, String token) {
        DtUsuario requestedBy = userService.getUserByToken(token);

        try {
            List<Curso> cursos = CSVHelper.deCSVaCurso(file);
            List<DtCurso> retorno = new ArrayList<DtCurso>();
            if(requestedBy.getTipoUsu() == 'A'){
                repository.saveAll(cursos);

                for(Curso c : cursos){
                    retorno.add(c.getDataType());
                }
                return retorno;
            }else{
                DtCurso c = new DtCurso();
                c.setError("No eres Administrador");
                retorno.add(c);
                return retorno;
            }
        } catch (Exception e) {
            throw new RuntimeException("Error al guardar el CSV: " + e.getMessage());
        }
    }

    public boolean bajaCurso(int id, String token) {
        DtUsuario requestedBy = userService.getUserByToken(token);

        if(requestedBy.getTipoUsu() == 'A'){
            try {
                Curso c = repository.findBy_id(id);

                List<DocenteCurso> inscripcionesDocente = c.getDocentesAsignados();
                List<Inscripcion> inscripcionesEstudiante = c.getInscriptos();

                for(DocenteCurso dc : inscripcionesDocente){
                    docCurRepository.delete(dc);
                }
                for(Inscripcion in : inscripcionesEstudiante){
                    inscRepository.delete(in);
                }

                List<Foro> foros = c.getForos();
                List<Tarea> tareas = c.getTareas();
                List<Libro> libros = c.getLibros();

                for(Foro f : foros){
                    foroService.bajaForo(f.get_id(), id);
                }
                for(Tarea t : tareas){
                    tareaService.bajaTarea(t.get_id(),id);
                }
                for(Libro l : libros){
                    libroService.bajaLibro(l.get_id(),token);
                }

                repository.delete(repository.findBy_id(id)); // Solo se podria desactivar
                return true;
            } catch (Exception e) {
                return false;
            }
        }else{
            return false;
        }
    }

    public DtCurso editarCurso(int id, Curso curso, String token) {
        DtUsuario requestedBy = userService.getUserByToken(token);

        if(requestedBy.getTipoUsu() == 'A'){
            var c = repository.findBy_id(id);

            c.setNombre(curso.getNombre());
            c.setDescripcion(curso.getDescripcion());
            c.setCreditos(curso.getCreditos());
            c.setFechaInicio(curso.getFechaInicio());    
            c.setColor(curso.getColor());   
            
            return repository.save(c).getDataType();
        }else{
            DtCurso c = new DtCurso();
            c.setError("No eres Administrador");
            return c;
        }
    }

    public List<DtCursoConNota> getCursosByUsuario(String mail, String token) {
        List<DtCursoConNota> dtCursos = new ArrayList<>();
        DtUsuario requestedBy = userService.getUserByToken(token);
        
        if(requestedBy.getMail().equals(mail) || requestedBy.getTipoUsu() == 'A'){
            List<Curso> cursos = repository.findAll();
            Usuario u = (Usuario) userRepository.findByMail(mail);
            DtUsuario user = u.getDataTypeBase();
        
            for(Curso c : cursos){
                if(user.getTipoUsu() == 'E'){
                    List<Inscripcion> inscripciones = c.getInscriptos();
                    for(Inscripcion i : inscripciones){
                        Estudiante e = i.getEstudiante();
                        if(e.getMail().equals(mail)){
                            DtCursoConNota cur = new DtCursoConNota();
                            cur.setCreditos(c.getCreditos());
                            cur.setDescripcion(c.getDescripcion());
                            cur.setFechaInicio(c.getFechaInicio());
                            cur.setNombre(c.getNombre());
                            cur.set_id(c.get_id());
                            cur.set_idInscripcion(i.get_id());
                            cur.setNota(i.getNotaFinal());
                            cur.setColor(c.getColor());
                            dtCursos.add(cur);
                        }
                    }
                }else if(user.getTipoUsu() == 'D'){
                    List<DocenteCurso> docentes = c.getDocentesAsignados();
                    for(DocenteCurso dc : docentes){
                        Docente d = dc.getDocente();
                        if(d.getMail().equals(mail)){
                            DtCursoConNota cur = new DtCursoConNota();
                            cur.setCreditos(c.getCreditos());
                            cur.setDescripcion(c.getDescripcion());
                            cur.setFechaInicio(c.getFechaInicio());
                            cur.setNombre(c.getNombre());
                            cur.setColor(c.getColor());
                            cur.set_id(c.get_id());
                            cur.set_idInscripcion(dc.get_id());
                            dtCursos.add(cur);
                        }
                    }
                }
            }
        }

        return dtCursos;
    }

    public boolean estaUsuarioEnCurso(String mailUsuario, int idCurso){
        boolean toReturn = false;

        try{
            Curso c = repository.findBy_id(idCurso);
            
            toReturn = estaUsuarioEnCurso(mailUsuario, c);

        }catch(Exception exception){
            toReturn = false;
        }


        return toReturn;
    }
    public boolean estaUsuarioEnCurso(String mailUsuario, Curso c){
        boolean toReturn = false;

        try{
            DtUsuario u = ((Usuario) userRepository.findByMail(mailUsuario)).getDataTypeBase();
            
            if(u.getTipoUsu() == 'D'){
                List<DocenteCurso> docentes = c.getDocentesAsignados();
                int i = 0;
                while(i < docentes.size() && !toReturn){
                    DocenteCurso docente = docentes.get(i);
                    if(docente.getDocente().getMail().equals(mailUsuario)){
                        toReturn = true;
                    }
                    i++;
                }
            }else if(u.getTipoUsu() == 'E'){
                List<Inscripcion> estudiantes = c.getInscriptos();
                int i = 0;
                while(i < estudiantes.size() && !toReturn){
                    Inscripcion estudiante = estudiantes.get(i);
                    if(estudiante.getEstudiante().getMail().equals(mailUsuario)){
                        toReturn = true;
                    }
                    i++;
                }
            }
        }catch(Exception exception){
            toReturn = false;
        }

        return toReturn;
    }

}
