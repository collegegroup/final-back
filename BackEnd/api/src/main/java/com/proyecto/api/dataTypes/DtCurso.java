package com.proyecto.api.dataTypes;

import java.util.Date;

public class DtCurso {

    private int _id;
    private String nombre;
    private String descripcion;
    private Integer creditos;
    private Date fechaInicio;
    private String color;
    private String error = "NONE";

    public DtCurso(){}
    
    public DtCurso(int _id, String nombre, String descripcion, Integer creditos,  Date fechaInicio, String color) {
        this._id = _id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.creditos = creditos;
        this.fechaInicio = fechaInicio;
        this.color = color;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getCreditos() {
        return creditos;
    }

    public void setCreditos(Integer creditos) {
        this.creditos = creditos;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    

}
