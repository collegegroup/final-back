package com.proyecto.api.repository.jpa;

import com.proyecto.api.model.Usuario;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario,String>{
    Usuario findBy_id(int _id);
    Usuario findByMail(String Mail);
}
