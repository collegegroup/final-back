package com.proyecto.api.controller;

import com.proyecto.api.model.Entrega;
import com.proyecto.api.service.EntregaService;
import com.proyecto.api.util.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.web.bind.annotation.RequestHeader;
import java.util.Map;

@RestController
@CrossOrigin 
@RequestMapping("/entregas")
public class EntregaController {

  @Autowired
  private EntregaService service;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<Object> getAllEntregas() {
      new Log("[Inicio] Obtener Entregas",Thread.currentThread()); //Log
      ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getAllEntregas(), HttpStatus.OK);
      new Log("[Fin] Obtener Entregas",Thread.currentThread()); //Log
      return toReturn;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getEntregaById(@PathVariable("id") Integer id) {
      new Log("[Inicio] Obtener Entrega por ID",Thread.currentThread()); //Log
      ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getEntregaById(id), HttpStatus.OK);   
      new Log("[Fin] Obtener Entrega por ID",Thread.currentThread()); //Log   
      return toReturn;  
    }

    //ENDPOINTS APP

    @RequestMapping(value = "/editarEntrega/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> editarEntrega(@PathVariable("id") Integer id, @Validated @RequestBody Entrega entrega) {
      new Log("[Inicio] Editar Entrega",Thread.currentThread()); //Log
      ResponseEntity<Object> toReturn = new ResponseEntity<>(service.editarEntrega(id, entrega), HttpStatus.OK);
      new Log("[Fin] Editar Entrega",Thread.currentThread()); //Log
      return toReturn;  
    }

    @RequestMapping(value = "/altaEntrega/{tareaId}", method = RequestMethod.POST)
    public ResponseEntity<Object> altaEntrega(@Validated @RequestBody ObjectNode jsonData, @PathVariable("tareaId") int tareaId, @RequestHeader Map<String,String> headers) {
      new Log("[Inicio] Alta Entrega",Thread.currentThread()); //Log
      ResponseEntity<Object> toReturn = null;

      String token = headers.get("authorization").substring(7);
      String user = jsonData.get("mailUser").toString().replace("\"", "");
      String linkEntrega = jsonData.get("link").toString().replace("\"", "");

      toReturn = new ResponseEntity<>(service.altaEntrega(linkEntrega, user, tareaId, token), HttpStatus.OK);
      new Log("[Fin] Alta Entrega",Thread.currentThread()); //Log
      return toReturn;
    }

    @RequestMapping(value = "/bajaEntrega/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> bajaEntrega(@PathVariable("id") Integer id) {
      new Log("[Inicio] Baja Entrega",Thread.currentThread()); //Log
      ResponseEntity<Object> toReturn = new ResponseEntity<>(service.bajaEntrega(id), HttpStatus.OK);    
      new Log("[Fin] Baja Entrega",Thread.currentThread()); //Log    
      return toReturn;
    }

    @RequestMapping(value = "/byUsuario/{mailUsuario}", method = RequestMethod.GET)
    public ResponseEntity<Object> byUsuario(@PathVariable("mailUsuario") String mailUsuario) {
      new Log("[Inicio] Obtener Entregas por Usuario",Thread.currentThread()); //Log
      ResponseEntity<Object> toReturn = new ResponseEntity<>(service.byUsuario(mailUsuario), HttpStatus.OK);        
      new Log("[Fin] Obtener Entregas por Usuario",Thread.currentThread()); //Log
      return toReturn;
    }

    @RequestMapping(value = "/byCurso/{idCurso}", method = RequestMethod.GET)
    public ResponseEntity<Object> byCurso(@PathVariable("idCurso") int idCurso) {
      new Log("[Inicio] Obtener Entregas por Curso",Thread.currentThread()); //Log
      ResponseEntity<Object> toReturn = new ResponseEntity<>(service.byCurso(idCurso), HttpStatus.OK);        
      new Log("[Fin] Obtener Entregas por Curso",Thread.currentThread()); //Log
      return toReturn;
    }

    @RequestMapping(value = "/calificarEntrega/{id}/{nota}", method = RequestMethod.PUT)
    public ResponseEntity<Object> calificarEntrega(@PathVariable("nota") int nota, @PathVariable("id") int id){
      new Log("[Inicio] Calificar Entrega",Thread.currentThread()); //Log
      ResponseEntity<Object> toReturn = new ResponseEntity<>(service.calificarEntrega(id, nota), HttpStatus.OK);
      new Log("[Fin] Calificar Entrega",Thread.currentThread()); //Log
      return toReturn;
    }

    //Entregas por Tarea (TODAS)
    @RequestMapping(value = "/byTarea/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getEntregaByTarea(@PathVariable("id") Integer id) {
      new Log("[Inicio] Obtener Entrega por ID de Tarea",Thread.currentThread()); //Log
      ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getEntregaByIdTarea(id), HttpStatus.OK);   
      new Log("[Fin] Obtener Entrega por ID de Tarea",Thread.currentThread()); //Log   
      return toReturn;  
    }

    //Entregas por Tarea (NO CALIFICADAS)
    @RequestMapping(value = "/sinCalificar/byTarea/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getEntregaSinCalificarByTarea(@PathVariable("id") Integer id) {
      new Log("[Inicio] Obtener Entrega Sin Calificar por ID de Tarea",Thread.currentThread()); //Log
      ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getEntregaSinCalificarByIdTarea(id), HttpStatus.OK);   
      new Log("[Fin] Obtener Entrega Sin Calificar por ID de Tarea",Thread.currentThread()); //Log   
      return toReturn;  
    }

}
