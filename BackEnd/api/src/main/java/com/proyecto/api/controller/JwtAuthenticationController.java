package com.proyecto.api.controller;

import com.proyecto.api.dataTypes.DtUsuario;
import com.proyecto.api.model.JwtRequest;
import com.proyecto.api.model.JwtResponse;
import com.proyecto.api.model.Usuario;
import com.proyecto.api.repository.jpa.UsuarioRepository;
import com.proyecto.api.service.JwtUserDetailsService;
import com.proyecto.api.util.JwtTokenUtil;
import com.proyecto.api.util.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class JwtAuthenticationController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;

	@Autowired
	private UsuarioRepository repository;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
		new Log("[Inicio] Inicio de Sesion",Thread.currentThread()); //Log
        authenticate(authenticationRequest.getUsername().toLowerCase(), authenticationRequest.getPassword());

		final UserDetails userDetails = userDetailsService
				.loadUserByUsername(authenticationRequest.getUsername().toLowerCase());

		final String token = jwtTokenUtil.generateToken(userDetails);
		
		if(!authenticationRequest.getToken().equals("")){
			Usuario u = repository.findByMail(userDetails.getUsername());
			u.setToken(authenticationRequest.getToken());
			repository.save(u);
		}		

		DtUsuario usuario = repository.findByMail(userDetails.getUsername()).getDataTypeBase();		

		ResponseEntity<?> toReturn = ResponseEntity.ok(new JwtResponse(token,usuario));
		new Log("[Fin] Inicio de Sesion",Thread.currentThread()); //Log
        
		return toReturn;
	}
	
	private void authenticate(String mail, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(mail, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
}