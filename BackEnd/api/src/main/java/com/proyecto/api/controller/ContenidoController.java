package com.proyecto.api.controller;

import java.util.Map;

import com.proyecto.api.model.Contenido;
import com.proyecto.api.service.ContenidoService;
import com.proyecto.api.util.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/contenidos")
public class ContenidoController {

    @Autowired
    private ContenidoService service;

    // TESTING
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<Object> getAllContenidos() {
        new Log("[Inicio] Obteniendo contenidos",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getAllContenidos(), HttpStatus.OK);
        new Log("[Fin] Obteniendo contenidos",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getContenidoById(@PathVariable("id") Integer id) {
        new Log("[Inicio] Obteniendo contenidos por ID ",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getContenidoById(id), HttpStatus.OK);
        new Log("[Fin] Obteniendo contenidos por ID",Thread.currentThread()); //Log
        return toReturn;
    }

    // ENDPOINTS APP
    
    @RequestMapping(value = "/libro/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getContenidoByLibroId(@PathVariable("id") Integer id, @RequestHeader Map<String,String> headers) {
        new Log("[Inicio] Obteniendo contenidos por ID de Libro",Thread.currentThread()); //Log
        String token = headers.get("authorization").substring(7);
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getContenidoByLibroId(id,token), HttpStatus.OK);
        new Log("[Fin] Obteniendo contenidos por ID de Libro",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/altaContenido/{id}", method = RequestMethod.POST)
    public ResponseEntity<Object> altaContenido(@PathVariable("id") Integer id,@Validated @RequestBody Contenido contenido){
        new Log("[Inicio] Alta Contenido en Libro",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.altaContenido(contenido, id), HttpStatus.OK);
        new Log("[Fin] Alta Contenido en Libro",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/bajaContenido/{id}/{idLibro}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> bajaContenido(@PathVariable("id") Integer id, @PathVariable("idLibro") Integer idLibro) {
        new Log("[Inicio] Baja Contenido de Libro",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.bajaContenido(id, idLibro), HttpStatus.OK);
        new Log("[Fin] Baja Contenido de Libro",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/editarContenido/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> EditarContenido(@PathVariable("id") Integer id,@Validated @RequestBody Contenido contenido) {
        new Log("[Inicio] Editar Contenido",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.editarContenido(id, contenido), HttpStatus.OK);
        new Log("[Fin] Editar Contenido",Thread.currentThread()); //Log
        return toReturn;
    }

}
