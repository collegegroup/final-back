package com.proyecto.api.controller;

import com.proyecto.api.model.Libro;
import com.proyecto.api.service.LibroService;
import com.proyecto.api.util.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin
@RequestMapping("/libros")
public class LibroController {
    
    @Autowired
    private LibroService service;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<Object> getAllLibros(@RequestHeader Map<String,String> headers) {
        new Log("[Inicio] Obtener Libros",Thread.currentThread()); //Log
        String token = headers.get("authorization").substring(7);
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getAllLibros(token), HttpStatus.OK);
        new Log("[Fin] Obtener Libros",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getLibroById(@PathVariable("id") int id,@RequestHeader Map<String,String> headers) {
        new Log("[Inicio] Obtener Libro por ID",Thread.currentThread()); //Log
        String token = headers.get("authorization").substring(7);
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getLibroById(id,token), HttpStatus.OK);
        new Log("[Fin] Obtener Libro por ID",Thread.currentThread()); //Log
        return toReturn;
    }

    // ENDPOINTS APP

    @RequestMapping(value = "/altaLibro/{id}", method = RequestMethod.POST)
    public ResponseEntity<Object> altaLibro(@PathVariable("id") int id, @Validated @RequestBody ObjectNode jsonData, @RequestHeader Map<String,String> headers) {
        new Log("[Inicio] Alta Libro",Thread.currentThread()); //Log
        String token = headers.get("authorization").substring(7);
        Libro libro = new Libro();

        try {
            libro = new ObjectMapper().readValue(jsonData.get("Libro").toString(), Libro.class);
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.altaLibro(id, libro, token), HttpStatus.OK);
        new Log("[Fin] Alta Libro",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/bajaLibro/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> bajaLibro(@PathVariable("id") int id, @RequestHeader Map<String,String> headers) {
        new Log("[Inicio] Baja Libro",Thread.currentThread()); //Log
        String token = headers.get("authorization").substring(7);
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.bajaLibro(id, token), HttpStatus.OK);
        new Log("[Fin] Baja Libro",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/editarLibro/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> editarLibro(@PathVariable("id") int id, @Validated @RequestBody ObjectNode jsonData, @RequestHeader Map<String,String> headers) {
        new Log("[Inicio] Editar Libro",Thread.currentThread()); //Log
        Libro libro = new Libro();
        String token = headers.get("authorization").substring(7);
        try {
            libro = new ObjectMapper().readValue(jsonData.get("Libro").toString(), Libro.class);
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.editarLibro(id, libro, token), HttpStatus.OK);
        new Log("[Fin] Editar Libro",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/curso/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getLibrosByCurso(@PathVariable("id") int id, @RequestHeader Map<String,String> headers) {
        new Log("[Inicio] Obtener Libros de Curso",Thread.currentThread()); //Log
        String token = headers.get("authorization").substring(7);
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getLibrosByCurso(id,token), HttpStatus.OK);
        new Log("[Fin] Obtener Libros de Curso",Thread.currentThread()); //Log
        return toReturn;
    }
}
