package com.proyecto.api.controller;

import java.util.Map;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.proyecto.api.model.Administrador;
import com.proyecto.api.model.Docente;
import com.proyecto.api.model.Estudiante;
import com.proyecto.api.model.Usuario;
import com.proyecto.api.service.JwtUserDetailsService;
import com.proyecto.api.service.UsuarioService;
import com.proyecto.api.util.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService service;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    // TESTING
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<Object> getAllUsers(@RequestHeader Map<String, String> headers) {
        new Log("[Inicio] Obtener Usuarios", Thread.currentThread()); // Log
        String token = headers.get("authorization").substring(7);
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getAllUsers(token), HttpStatus.OK);
        new Log("[Fin] Obtener Usuarios", Thread.currentThread()); // Log
        return toReturn;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getUserById(@PathVariable("id") int id, @RequestHeader Map<String, String> headers) {
        new Log("[Inicio] Obtener Usuario por ID", Thread.currentThread()); // Log
        String token = headers.get("authorization").substring(7);
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getUserById(id, token), HttpStatus.OK);
        new Log("[Fin] Obtener Usuario por ID", Thread.currentThread()); // Log
        return toReturn;
    }

    // ENDPOINTS APP

    @RequestMapping(value = "/altaEstudiante", method = RequestMethod.POST)
    public ResponseEntity<?> saveUser(@RequestBody Estudiante user, @RequestHeader Map<String, String> headers)
            throws Exception {
        new Log("[Inicio] Alta Estudiante", Thread.currentThread()); // Log
        String token = headers.get("authorization").substring(7);
        user.setMail(user.getMail().toLowerCase());
        ResponseEntity<Object> toReturn = ResponseEntity.ok(userDetailsService.save(user, token));
        new Log("[Fin] Alta Estudiante", Thread.currentThread()); // Log
        return toReturn;
    }

    @RequestMapping(value = "/altaDocente", method = RequestMethod.POST)
    public ResponseEntity<?> saveUser(@RequestBody Docente user, @RequestHeader Map<String, String> headers)
            throws Exception {
        new Log("[Inicio] Alta Docente", Thread.currentThread()); // Log
        String token = headers.get("authorization").substring(7);
        user.setMail(user.getMail().toLowerCase());
        ResponseEntity<Object> toReturn = ResponseEntity.ok(userDetailsService.save(user, token));
        new Log("[Fin] Alta Docente", Thread.currentThread()); // Log
        return toReturn;
    }

    @RequestMapping(value = "/altaAdministrador", method = RequestMethod.POST)
    public ResponseEntity<?> saveUser(@RequestBody Administrador user, @RequestHeader Map<String, String> headers)
            throws Exception {
        String token = "";
        new Log("[Inicio] Alta Administrador", Thread.currentThread()); // Log
        if (headers.get("authorization") != null) {
            token = headers.get("authorization").substring(7);
        }
        user.setMail(user.getMail().toLowerCase());
        ResponseEntity<Object> toReturn = ResponseEntity.ok(userDetailsService.save(user, token));
        new Log("[Inicio] Alta Administrador", Thread.currentThread()); // Log
        return toReturn;
    }

    // CARGA MASIVA
    @RequestMapping(value = "/altaUsuarios/csv", method = RequestMethod.POST)
    public ResponseEntity<Object> cargaMasivaUsuarios(@RequestBody String csv,
            @RequestHeader Map<String, String> headers) {
        new Log("[Inicio] Alta Masiva Usuarios", Thread.currentThread()); // Log
        ResponseEntity<Object> toReturn; 
        String token = headers.get("authorization").substring(7);

        toReturn = new ResponseEntity<>(userDetailsService.saveMasivo(csv, token), HttpStatus.OK);

        new Log("[Fin] Alta Masiva Usuarios",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/editarPerfil/{mailOriginal}", method = RequestMethod.PUT)
    public ResponseEntity<Object> EditarPerfil(@PathVariable("mailOriginal") String mailOriginal, @Validated @RequestBody Usuario usuario , @RequestHeader Map<String,String> headers) {
        new Log("[Inicio] Editar Perfil",Thread.currentThread()); //Log

        mailOriginal = mailOriginal.toLowerCase();
        usuario.setMail(usuario.getMail().toLowerCase());
        String token = headers.get("authorization").substring(7);
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.EditarPerfil(mailOriginal, usuario, token), HttpStatus.OK);
        
        new Log("[Fin] Editar Perfil",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/editarPerfilAdmin/{mailOriginal}", method = RequestMethod.PUT)
    public ResponseEntity<Object> EditarPerfilAdmin(@PathVariable("mailOriginal") String mailOriginal, @Validated @RequestBody Usuario usuario, @RequestHeader Map<String,String> headers) {
        new Log("[Inicio] Editar Perfil como Administrador",Thread.currentThread()); //Log

        mailOriginal = mailOriginal.toLowerCase();
        usuario.setMail(usuario.getMail().toLowerCase());
        String token = headers.get("authorization").substring(7);
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.EditarPerfilAdmin(mailOriginal, usuario, token), HttpStatus.OK);
        
        new Log("[Fin] Editar Perfil como Administrador",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/cambiarPass", method = RequestMethod.PUT)
    public ResponseEntity<Object> CambiarPass(@Validated @RequestBody ObjectNode jsonData) {
        new Log("[Inicio] Cambiar Contraseña",Thread.currentThread()); //Log
        
        String email = jsonData.get("mail").toString().toLowerCase();
        String oldPass = jsonData.get("oldpass").toString();
        String newPass1 = jsonData.get("newpass").toString();
        String newPass2 = newPass1;
        
        email = email.substring(0, email.length()-1);email = email.substring(1);
        oldPass = oldPass.substring(0, oldPass.length()-1);oldPass = oldPass.substring(1);
        newPass1 = newPass1.substring(0, newPass1.length()-1);newPass1 = newPass1.substring(1);
        newPass2 = newPass2.substring(0, newPass2.length()-1);newPass2 = newPass2.substring(1);

        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.CambiarPass(email, oldPass, newPass1, newPass2), HttpStatus.OK);
        
        new Log("[Fin] Cambiar Contraseña",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/recuperarContra/{email}", method = RequestMethod.POST)
    public ResponseEntity<Object> RecuperarContra(@PathVariable("email") String email) {
        new Log("[Inicio] Recuperar Contraseña",Thread.currentThread()); //Log
        email = email.toLowerCase();
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.RecuperarContra(email), HttpStatus.OK);
        new Log("[Fin] Recuperar Contraseña",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/{email}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> bajaUsuario(@PathVariable("email") String email, @RequestHeader Map<String,String> headers) {
        new Log("[Inicio] Baja Usuario",Thread.currentThread()); //Log
        String token = headers.get("authorization").substring(7);
        email = email.toLowerCase();
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.bajaUsuario(email, token), HttpStatus.OK);
        new Log("[Fin] Baja Usuario",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/getUsuario/{token}", method = RequestMethod.GET)
    public ResponseEntity<Object> getUserByToken(@PathVariable("token") String token) {
        new Log("[Inicio] Obtener Usuario por Token",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getUserByToken(token), HttpStatus.OK);
        new Log("[Fin] Obtener Usuario por Token",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/getEstudiantesByCurso/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getEstudiantesByCurso(@PathVariable("id") int id, @RequestHeader Map<String,String> headers) {
        new Log("[Inicio] Obtener Estudiantes de Curso",Thread.currentThread()); //Log
        String token = headers.get("authorization").substring(7);
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getEstudiantesByCurso(id, token), HttpStatus.OK);
        new Log("[Fin] Obtener Estudiantes de Curso",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/getDocentesByCurso/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getDocentesByCurso(@PathVariable("id") int id, @RequestHeader Map<String,String> headers) {
        new Log("[Inicio] Obtener Docentes de Curso",Thread.currentThread()); //Log
        String token = headers.get("authorization").substring(7);
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getDocentesByCurso(id, token), HttpStatus.OK);
        new Log("[Fin] Obtener Docentes de Curso",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/getUsuarioTipo/{tipo}", method = RequestMethod.GET)
    public ResponseEntity<Object> getUsuarioTipo(@PathVariable("tipo") char id, @RequestHeader Map<String,String> headers) {
        new Log("[Inicio] Obtener Tipo Usuario",Thread.currentThread()); //Log
        String token = headers.get("authorization").substring(7);
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getUsuarioTipo(id, token), HttpStatus.OK);
        new Log("[Fin] Obtener Tipo Usuario",Thread.currentThread()); //Log
        return toReturn;
    }

}
