package com.proyecto.api.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.proyecto.api.model.Administrador;
import com.proyecto.api.model.Curso;
import com.proyecto.api.model.Docente;
import com.proyecto.api.model.Estudiante;
import com.proyecto.api.model.Usuario;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.multipart.MultipartFile;
import java.util.Base64;

public class CSVHelper {

    public static String type = "text/csv";
    static String[] headerCurso = { "Nombre", "Descripcion", "Creditos", "FechaInicio" };
    static String[] headerUsuario = { "Nombre", "Mail", "Tipo", "Direccion", "Descripcion", "TipoDocumento", "Documento" ,"Carrera"};
    static String[] headerEstudiante = { "Nombre", "Mail", "Direccion", "Descripcion", "TipoDocumento", "Documento", "Carrera" };
    static String[] headerAsignacionEstudiante = { "EstudianteId", "CursoId", "NotaFinal", "FechaInicial", "FechaFinal" };
    static String[] headerAsignacionDocente = { "DocenteId", "CursoId", "Horario" };

    @Autowired
	private static PasswordEncoder bcryptEncoder = new BCryptPasswordEncoder();
  
    public static boolean hasCSVFormat(MultipartFile file) {

      if (!type.equals(file.getContentType())) {
        return false;
      }
  
      return true;
    }
  
    public static List<Curso> deCSVaCurso(String is) {
      try {
        is = is.split("base64,")[1];
        byte[] decodedString = Base64.getDecoder().decode(is.getBytes("UTF-8"));
        InputStream targetStream = new ByteArrayInputStream(decodedString);

        BufferedReader fileReader = new BufferedReader(new InputStreamReader(targetStream, "UTF-8"));
        CSVParser csvParser = new CSVParser(fileReader, CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
  
        List<Curso> cursos = new ArrayList<Curso>();
  
        Iterable<CSVRecord> csvRecords = csvParser.getRecords();
  
        for (CSVRecord csvRecord : csvRecords) {
          Curso curso = new Curso();
          curso.setNombre(csvRecord.get("Nombre"));
          curso.setDescripcion(csvRecord.get("Descripcion"));
          curso.setCreditos(Integer.parseInt(csvRecord.get("Creditos")));
          curso.setFechaInicio(new Date(Date.parse(csvRecord.get("FechaInicio"))));
          curso.setColor("0xFF005377");
  
          cursos.add(curso);
        }
  
        return cursos;
      } catch (IOException e) {
        throw new RuntimeException("Error al parsear el archivo: " + e.getMessage());
      }
    }

    public static List<Usuario> deCSVaUsuarios(String is) {
        try {
          is = is.split("base64,")[1];
          byte[] decodedString = Base64.getDecoder().decode(is.getBytes("UTF-8"));
          InputStream targetStream = new ByteArrayInputStream(decodedString);

          BufferedReader fileReader = new BufferedReader(new InputStreamReader(targetStream, "UTF-8"));
          CSVParser csvParser = new CSVParser(fileReader, CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
           
            List<Usuario> usuarios = new ArrayList<Usuario>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                String key = csvRecord.get("Tipo");
                Usuario user = null;
                switch (key) {
                    case "E":
                        Estudiante aux = new Estudiante();
                        aux.setCarrera(csvRecord.get("Carrera"));
                        user = aux;
                        break;
                    case "A":
                        user = new Administrador();
                        break;
                    case "D":
                        user = new Docente();
                        break;
                    default:
                        throw new RuntimeException("No se a ingresado el tipo correcto.");
                }
            
                user.setNombre(csvRecord.get("Nombre"));
                user.setMail(csvRecord.get("Mail").toLowerCase());
                user.setDireccion(csvRecord.get("Direccion"));
                user.setDescripcion(csvRecord.get("Descripcion"));
                user.setTipoDocumento(csvRecord.get("TipoDocumento"));
                user.setDocumento(csvRecord.get("Documento"));
                user.setPassword(bcryptEncoder.encode("Sap.io2020"));

                //no hago envio de mail
                
                usuarios.add(user);
            }
            return usuarios;
        } catch (IOException e) {
            throw new RuntimeException("Error al parsear el archivo: " + e.getMessage());
        }
  
    }
}