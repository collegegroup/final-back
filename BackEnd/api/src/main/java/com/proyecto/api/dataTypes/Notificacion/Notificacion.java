package com.proyecto.api.dataTypes.Notificacion;

import java.util.List;

public class Notificacion {
    public Mensaje notification;
    public String priority;
    public Data data;
    public List<String> registration_ids;
}