package com.proyecto.api.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.proyecto.api.dataTypes.Notificacion.Mensaje;
import com.proyecto.api.dataTypes.Notificacion.Notificacion;

import org.springframework.stereotype.Service;

@Service
public class NotificationsService {

    public static void SendNotifications(String title, String body, List<String> tokens) {
        Notificacion notificacion = new Notificacion();
        Mensaje mensaje = new Mensaje();
        mensaje.body = body;
        mensaje.title = title;
        notificacion.notification = mensaje;
        notificacion.priority = "high";
        // Data data = new Data();
        // data.click_action = "FLUTER_NOTIFICATION_CLICK";
        // data.id = "1";
        // data.done = "done";
        // data.nombre = "Nombre";
        //notificacion.data = data;
        notificacion.registration_ids = tokens;

        try {
            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Authorization", "Bearer " + "AAAAls_QX7Q:APA91bFB_n3b1t2DP8UJzQPZJCfhCFcOOsf6-O4ipeHjb-hP8SzbUbhd05uutGS2_Y_WzQm4U6PDbqIprAbOkJzPjnUaM8LB7USHZYLmuLQRX-QBPNQrhrGDCoAQt4hqkHAZxU80_pgh");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);
            ObjectMapper mapper = new ObjectMapper();
            String jsonString = mapper.writeValueAsString(notificacion);
            try (OutputStream os = con.getOutputStream()) {
                byte[] input = jsonString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                
                System.out.println(response.toString());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
