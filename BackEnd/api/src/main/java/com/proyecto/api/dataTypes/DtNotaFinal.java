package com.proyecto.api.dataTypes;

public class DtNotaFinal {
    
    private String mail;
    private double nota;

    public DtNotaFinal() {}
    public DtNotaFinal(String mail, double nota) {
        this.mail = mail;
        this.nota = nota;
    }

    public String getMail() {
        return mail;
    }
    public double getNota() {
        return nota;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
    public void setNota(double nota) {
        this.nota = nota;
    }

}
