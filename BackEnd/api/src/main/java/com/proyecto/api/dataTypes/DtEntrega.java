package com.proyecto.api.dataTypes;

import java.util.Date;

public class DtEntrega {

    private int _id;
    private DtUsuario estudiante;
    private DtTarea tarea;
    private Date fechaDeEntrega;
    private Integer nota;
    private String contenido;
    private String archivo;
    private String error = "NONE";

    public DtEntrega(){}
    
    public DtEntrega(int _id, DtUsuario estudiante, DtTarea tarea, Date fechaDeEntrega, Integer nota, String contenido,
            String archivo) {
        this._id = _id;
        this.estudiante = estudiante;
        this.tarea = tarea;
        this.fechaDeEntrega = fechaDeEntrega;
        this.nota = nota;
        this.contenido = contenido;
        this.archivo = archivo;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public DtUsuario getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(DtUsuario estudiante) {
        this.estudiante = estudiante;
    }

    public DtTarea getTarea() {
        return tarea;
    }

    public void setTarea(DtTarea tarea) {
        this.tarea = tarea;
    }

    public Date getFechaDeEntrega() {
        return fechaDeEntrega;
    }

    public void setFechaDeEntrega(Date fechaDeEntrega) {
        this.fechaDeEntrega = fechaDeEntrega;
    }

    public Integer getNota() {
        return nota;
    }

    public void setNota(Integer nota) {
        this.nota = nota;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
