package com.proyecto.api.dataTypes;

import java.util.Date;

public class DtCursoConNota {

    private int _id;
    private String nombre;
    private String descripcion;
    private Integer creditos;
    private Date fechaInicio;
    private String color;

    private int _idInscripcion;
    private double nota;

    public DtCursoConNota(){}
    
    public DtCursoConNota(int _id, String nombre, String descripcion, Integer creditos,  Date fechaInicio, int _idInscripcion, double nota) {
        this._id = _id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.creditos = creditos;
        this.fechaInicio = fechaInicio;
        this.nota = nota;
        this._idInscripcion = _idInscripcion;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getCreditos() {
        return creditos;
    }

    public void setCreditos(Integer creditos) {
        this.creditos = creditos;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public double getNota() {
        return nota;
    }

    public void setNota(double nota) {
        this.nota = nota;
    }

    public int get_idInscripcion() {
        return _idInscripcion;
    }

    public void set_idInscripcion(int _idInscripcion) {
        this._idInscripcion = _idInscripcion;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    

}
