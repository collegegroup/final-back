package com.proyecto.api.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.proyecto.api.dataTypes.DtInscripcion;

@Entity
@Table(name = "docentecursos", schema = "public")
public class DocenteCurso {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int _id;

    @ManyToOne
    @JoinColumn(name = "_docenteid")
    @JsonIgnoreProperties("docenteCursos")
    private Docente docente;

    @ManyToOne
    @JoinColumn(name = "_cursoid")
    @JsonIgnoreProperties("docentesAsignados")
    private Curso curso;

    @Column(name = "horario")
    private Date horario;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public Docente getDocente() {
        return docente;
    }

    public void setDocente(Docente docente) {
        this.docente = docente;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public Date getHorario() {
        return horario;
    }

    public void setHorario(Date horario) {
        this.horario = horario;
    }

    public DocenteCurso() {}

    public DocenteCurso(int _id, Docente docente, Curso curso, Date horario) {
        this._id = _id;
        this.docente = docente;
        this.curso = curso;
        this.horario = horario;
    }   

    
    public DtInscripcion getDataType(){
        DtInscripcion toReturn = new DtInscripcion();

        toReturn.setUsuarioId(docente.get_id());
        toReturn.setCursoId(curso.get_id());
        toReturn.setHorario(horario);
        toReturn.set_id(_id);

        return toReturn;
    }

}
