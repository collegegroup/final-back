package com.proyecto.api.config;

import com.proyecto.api.util.Log;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Controller;

@EnableAutoConfiguration
@EntityScan("com.proyecto.api.model")
@EnableJpaRepositories("com.proyecto.api.repository.jpa")
@ComponentScan(basePackages = {"com.proyecto.api.model","com.proyecto.api.controller","com.proyecto.api.service","com.proyecto.api.util","com.proyecto.api.config", "com.proyecto.api.datatypes"})
@SpringBootApplication
@Controller
public class ApiApplication extends SpringBootServletInitializer{

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application){
		Log.setSysStart(System.currentTimeMillis());
		new Log("System Started.",Thread.currentThread());
		return application.sources(ApiApplication.class);
	}

	public static void main(String[] args) {
		Log.setSysStart(System.currentTimeMillis());
		new Log("System Started.",Thread.currentThread());
		SpringApplication.run(ApiApplication.class, args);
	}

}
