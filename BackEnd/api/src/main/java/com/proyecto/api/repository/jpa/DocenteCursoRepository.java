package com.proyecto.api.repository.jpa;

import java.util.List;

import com.proyecto.api.model.DocenteCurso;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface DocenteCursoRepository extends JpaRepository<DocenteCurso,String>{

    DocenteCurso findBy_id(int _id);

    @Query(
        value = "SELECT * FROM docentecursos AS dc WHERE dc._docenteid = :docenteId", 
        nativeQuery = true)
    List<DocenteCurso> getInscripcionesDeDocente(@Param("docenteId") int docenteId);

}