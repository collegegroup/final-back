package com.proyecto.api.model;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.proyecto.api.dataTypes.DtUsuario;

@Entity
@DiscriminatorValue("E")
public class Estudiante extends Usuario {
    
    private String carrera;

    //////////////////////////////////////////////////// Relaciones ///////////////////////////////////

    @OneToMany(mappedBy = "estudiante")
    private List<Inscripcion> inscripcionCurso;

    @OneToMany(mappedBy = "estudiante")
    private List<Entrega> entregas;

    //////////////////////////////////////////////////// Fin de Bloque ////////////////////////////////

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public Estudiante(String carrera) {
        this.carrera = carrera;
    }

    public Estudiante(){
        
    }

    public Estudiante(int _id, String nombre, String mail, String pasword, String imagen, String direccion,
            String descripcion, String links, String tipoDocumento, String documento, String carrera) {
        super(_id, nombre, mail, pasword, imagen, direccion, descripcion, links, tipoDocumento, documento);
        this.carrera = carrera;
    }
    
    public void addEntrega(Entrega entrega) {
        this.entregas.add(entrega);
    }

    public void removeEntrega(Entrega entrega){
        this.entregas.remove(entrega);
    }

    public DtUsuario getDataType(){
        DtUsuario toReturn = this.getDataTypeBase();

        toReturn.setTipoUsu('E');
        toReturn.setCarrera(carrera);

        return toReturn;
    }

    public List<Inscripcion> getInscripcionCurso() {
        return inscripcionCurso;
    }

    public void setInscripcionCurso(List<Inscripcion> inscripcionCurso) {
        this.inscripcionCurso = inscripcionCurso;
    }

    public List<Entrega> getEntregas() {
        return entregas;
    }

    public void setEntregas(List<Entrega> entregas) {
        this.entregas = entregas;
    }

    
}
