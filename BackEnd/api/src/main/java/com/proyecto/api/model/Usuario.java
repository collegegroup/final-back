package com.proyecto.api.model;

import java.lang.reflect.Field;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.proyecto.api.dataTypes.DtUsuario;

@Entity
@Table(name = "usuarios", schema = "public")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int _id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "mail")
    private String mail;

    @Column(name = "password")
    private String password;

    @Column(name = "imagen")
    private String imagen;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "tipoDocumento")
    private String tipoDocumento;

    @Column(name = "documento")
    private String documento;

    @Column(name = "token")
    private String token;

    // Agregamos visibilidad a mensajes en foros ?
    /*
     * @OneToMany(mappedBy = "usuario") private List<Mensaje> mensajes;
     */

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String pasword) {
        this.password = pasword;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    public Usuario() {
    }

    public Usuario(int _id, String nombre, String mail, String password, String imagen, String direccion,
            String descripcion, String links, String tipoDocumento, String documento) {
        this._id = _id;
        this.nombre = nombre;
        this.mail = mail;
        this.password = password;
        this.imagen = imagen;
        this.direccion = direccion;
        this.descripcion = descripcion;
        this.tipoDocumento = tipoDocumento;
        this.documento = documento;
    }

    public DtUsuario getDataTypeBase() {
        DtUsuario toReturn = new DtUsuario();
        String clase = this.getClass().getSimpleName();
        switch (clase) {
            case "Estudiante":
                toReturn.setTipoUsu('E');
                try {
                    Field field = this.getClass().getDeclaredField("carrera");
                    field.setAccessible(true);
                    Object carrera = field.get(this);
                    toReturn.setCarrera(carrera.toString());
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            case "Administrador":
                toReturn.setTipoUsu('A');
                break;
            case "Docente":
                toReturn.setTipoUsu('D');
                break;
            default:
                toReturn.setTipoUsu('U');
        }
        toReturn.setDescripcion(descripcion);
        toReturn.setDireccion(direccion);
        toReturn.setDocumento(documento);
        toReturn.setImagen(imagen);
        toReturn.setMail(mail);
        toReturn.setNombre(nombre);
        toReturn.setTipoDocumento(tipoDocumento);
        toReturn.set_id(_id);

        return toReturn;
    }

}
