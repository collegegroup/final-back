package com.proyecto.api.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.proyecto.api.dataTypes.DtInscripcion;
import com.proyecto.api.dataTypes.DtNotaFinal;

@Entity
@Table(name = "inscripciones", schema = "public")
public class Inscripcion {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int _id;

    @ManyToOne
    @JoinColumn(name = "_estudianteid")
    @JsonIgnoreProperties("inscripcionCurso")
    private Estudiante estudiante;

    @ManyToOne
    @JoinColumn(name = "_cursoid")
    @JsonIgnoreProperties("inscriptos")
    private Curso curso;

    @Column(name = "notafinal")
    private Double notaFinal;

    @Column(name = "fechainicial")
    private Date fechaInicial;

    @Column(name = "fechafinal")
    private Date fechaFinal;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public Double getNotaFinal() {
        return notaFinal;
    }

    public void setNotaFinal(Double notaFinal) {
        this.notaFinal = notaFinal;
    }

    public Date getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public Inscripcion() {
    }

    public Inscripcion(int _id, Estudiante estudiante, Curso curso, Double notaFinal, Date fechaInicial, Date fechaFinal) {
        this._id = _id;
        this.estudiante = estudiante;
        this.curso = curso;
        this.notaFinal = notaFinal;
        this.fechaInicial = fechaInicial;
        this.fechaFinal = fechaFinal;
    }

    public DtInscripcion getDataType(){
        DtInscripcion toReturn = new DtInscripcion();

        toReturn.setUsuarioId(estudiante.get_id());
        toReturn.setCursoId(curso.get_id());
        toReturn.setNotaFinal(notaFinal);
        toReturn.setFechaInicial(fechaInicial);
        toReturn.setFechaFinal(fechaFinal);
        toReturn.set_id(_id);

        return toReturn;
    }
    
    public DtNotaFinal getDtNotaFinal(){
        DtNotaFinal toReturn = new DtNotaFinal();

        toReturn.setMail(estudiante.getMail());
        toReturn.setNota(notaFinal);

        return toReturn;
    }

}
