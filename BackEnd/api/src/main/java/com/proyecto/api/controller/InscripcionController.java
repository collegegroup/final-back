package com.proyecto.api.controller;

import com.proyecto.api.service.InscripcionService;
import com.proyecto.api.util.Log;

import java.util.List;
import java.util.Map;

import com.proyecto.api.model.DocenteCurso;
import com.proyecto.api.model.Inscripcion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import com.fasterxml.jackson.databind.node.ObjectNode;

@RestController
@CrossOrigin
@RequestMapping("/inscripciones")
public class InscripcionController {
    @Autowired
    private InscripcionService service;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<Object> getAllInscripciones(){
        new Log("[Inicio] Obtener Inscripciones",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getAllInscripciones(), HttpStatus.OK);
        new Log("[Fin] Obtener Inscripciones",Thread.currentThread()); //Log
        return toReturn;
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getInscripcionById(@PathVariable("id") int id){
        new Log("[Inicio] Obtener Inscripcione por ID",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getInscripcionById(id), HttpStatus.OK);
        new Log("[Fin] Obtener Inscripcione por ID",Thread.currentThread()); //Log
        return toReturn;
    }

    //metodos para estudiantes
    @RequestMapping(value = "/altaInscripcion/", method = RequestMethod.POST)
    public ResponseEntity<?> saveInscripcion(@Validated @RequestBody ObjectNode jsonData, @RequestHeader Map<String,String> headers) throws Exception {
        new Log("[Inicio] Alta Inscripcion",Thread.currentThread()); //Log
        
        String token = headers.get("authorization").substring(7);
        int cursoId = Integer.parseInt(jsonData.get("cursoId").toString());
        String usuarioEmail = jsonData.get("mailUsuario").toString();
        usuarioEmail = usuarioEmail.substring(1,usuarioEmail.length()-1);

        String datos [] = new String[4];
        datos [0] = jsonData.get("Inscripcion").get("notaFinal").toString();
        datos [1] = jsonData.get("Inscripcion").get("fechaInicial").toString();
        datos [2] = jsonData.get("Inscripcion").get("fechaFinal").toString();
        datos [3] = jsonData.get("Inscripcion").get("horario").toString();
        datos [3] = datos [3].substring(1,datos [3].length()-1);

        ResponseEntity<Object> toReturn = ResponseEntity.ok(service.saveInscripcion(datos, cursoId, usuarioEmail, token));
        new Log("[Fin] Alta Inscripcion",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/bajaInscripcion/", method = RequestMethod.POST)
    public ResponseEntity<?> bajaInscripcion(@Validated @RequestBody ObjectNode jsonData, @RequestHeader Map<String,String> headers) throws Exception {
        new Log("[Inicio] Baja Inscripcion",Thread.currentThread()); //Log
        
        String token = headers.get("authorization").substring(7);
        int cursoId = Integer.parseInt(jsonData.get("cursoId").toString());
        String usuarioEmail = jsonData.get("mailUsuario").toString();
        usuarioEmail = usuarioEmail.substring(1,usuarioEmail.length()-1);

        ResponseEntity<Object> toReturn = ResponseEntity.ok(service.bajaInscripcion(cursoId, usuarioEmail, token));
        new Log("[Fin] Baja Inscripcion",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/editarInscripcionEstudiante/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> editarInscripcion(@PathVariable("id") int id, @Validated @RequestBody Inscripcion inscripcion){
        new Log("[Inicio] Editar Inscripcion Estudiante",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.editarInscripcion(id, inscripcion), HttpStatus.OK);
        new Log("[Fin] Editar Inscripcion Estudiante",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/deleteInscripcionEstudiante/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> bajaInscripcion(@PathVariable("id") int id){
        new Log("[Inicio] Baja Inscripcion Estudiante",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.bajaInscripcionEstudiante(id), HttpStatus.OK);
        new Log("[Fin] Baja Inscripcion Estudiante",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/editarInscripcionDocente/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> editarInscripcion(@PathVariable("id") int id, @Validated @RequestBody DocenteCurso inscripcion){
        new Log("[Inicio] Editar Inscripcion Docente",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.editarInscripcion(id, inscripcion), HttpStatus.OK);
        new Log("[Fin] Editar Inscripcion Docente",Thread.currentThread()); //Log
        return toReturn;
    }

    @RequestMapping(value = "/deleteInscripcionDocente/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> bajaInscripcionDocente(@PathVariable("id") int id){
        new Log("[Inicio] Baja Inscripcion Docente",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.bajaInscripcionDocente(id), HttpStatus.OK);
        new Log("[Fin] Baja Inscripcion Docente",Thread.currentThread()); //Log
        return toReturn;
    }

    //Calificaciones
    @RequestMapping(value = "/calificacionFinal/{id}/{mail}/{nota}", method = RequestMethod.PUT)
    public ResponseEntity<Object> altaCalificacionFinal(@PathVariable("id") int id, @PathVariable("nota") Double calificacion, @PathVariable("mail") String mail){
        new Log("[Inicio] Alta Calificacion Final",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.altaCalificacionFinal(id, calificacion, mail), HttpStatus.OK);
        new Log("[Fin] Alta Calificacion Final",Thread.currentThread()); //Log
        return toReturn;
    }
    @RequestMapping(value = "/getCalificacionFinal/{id}/{mail}", method = RequestMethod.GET)
    public ResponseEntity<Object> getCalificacionFinal(@PathVariable("id") int id, @PathVariable("mail") String mail){
        new Log("[Inicio] Get Calificacion Final",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getCalificacionFinal(id, mail), HttpStatus.OK);
        new Log("[Fin] Get Calificacion Final",Thread.currentThread()); //Log
        return toReturn;
    }
    @RequestMapping(value = "/getCalificacionesCurso/{idCurso}", method = RequestMethod.GET)
    public ResponseEntity<Object> getCalificacionesCurso(@PathVariable("idCurso") int idCurso){
        new Log("[Inicio] Get Calificaciones Curso",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getCalificacionesCurso(idCurso), HttpStatus.OK);
        new Log("[Fin] Get Calificaciones Curso",Thread.currentThread()); //Log
        return toReturn;
    }

    //CARGA MASIVA ISNCRIPCION ESTUDIANTES
    @RequestMapping(value = "/altaMasivaInscripcion", method = RequestMethod.POST)
    public ResponseEntity<Object> altaMasivaInscripcion(@RequestBody Integer cursoId, @RequestBody List<String> usuarioEmails){
        new Log("[Inicio] Alta Masiva Inscripciones Estudiantes",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.cargaMasivaInscripcion(cursoId, usuarioEmails), HttpStatus.OK);
        new Log("[Fin] Alta Masiva Inscripciones Estudiantes",Thread.currentThread()); //Log
        return toReturn;
    }

    //CARGA MASIVA ISNCRIPCION DOCENTECURSO
    @RequestMapping(value = "/altaMasivaDocenteCurso", method = RequestMethod.POST)
    public ResponseEntity<Object> altaMasivaDocenteCurso(@RequestBody Integer cursoId, @RequestBody List<String> usuarioEmails){
        new Log("[Inicio] Alta Masiva Inscripciones Docentes",Thread.currentThread()); //Log
        ResponseEntity<Object> toReturn = new ResponseEntity<>(service.cargaMasivaDocenteCurso(cursoId, usuarioEmails), HttpStatus.OK);
        new Log("[Fin] Alta Masiva Inscripciones Docentes",Thread.currentThread()); //Log
        return toReturn;
    }
}