package com.proyecto.api.dataTypes;

import java.util.Date;

public class DtInscripcion {

    private int _id;
    private Integer usuarioId;
    private Integer cursoId;
    private double notaFinal;
    private Date fechaInicial;
    private Date fechaFinal;
    private Date horario;
    private String error = "NONE";

    public DtInscripcion() {}

    public DtInscripcion(int _id, Integer usuarioId, Integer cursoId, double notaFinal, Date fechaInicial,
            Date fechaFinal, Date horario) {
        this._id = _id;
        this.usuarioId = usuarioId;
        this.cursoId = cursoId;
        this.notaFinal = notaFinal;
        this.fechaInicial = fechaInicial;
        this.fechaFinal = fechaFinal;
        this.horario = horario;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public Integer getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Integer getCursoId() {
        return cursoId;
    }

    public void setCursoId(Integer cursoId) {
        this.cursoId = cursoId;
    }

    public double getNotaFinal() {
        return notaFinal;
    }

    public void setNotaFinal(double notaFinal) {
        this.notaFinal = notaFinal;
    }

    public Date getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public Date getHorario() {
        return horario;
    }

    public void setHorario(Date horario) {
        this.horario = horario;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    

}
