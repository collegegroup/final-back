package com.proyecto.api.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.proyecto.api.dataTypes.DtTarea;

@Entity
@Table(name = "tareas", schema = "public")
public class Tarea {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int _id;

    @Column(name = "fechaLimite")
    private Date fechaLimite;

    @Column(name = "titulo")
    private String titulo;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "entregable")
    private boolean entregable;

    @Column(name = "autoentregacalculada")
    private boolean AutoEntregaCalculada = false;

    @OneToMany(mappedBy = "tarea")
    private List<Entrega> entregas;

    public int get_id() {
        return _id;
    }

    public Date getFechaLimite() {
        return fechaLimite;
    }

    public void setFechaLimite(Date fechaLimite) {
        this.fechaLimite = fechaLimite;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Entrega> getEntregas() {
        return entregas;
    }

    public void setEntregas(List<Entrega> entregas) {
        this.entregas = entregas;
    }

    

    public Tarea() {
    }

    public Tarea(int _id, Date fechaLimite, String titulo, String descripcion, boolean entregable, List<Entrega> entregas) {
        this._id = _id;
        this.fechaLimite = fechaLimite;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.entregas = entregas;
        this.entregable = entregable;
    }

    public void addEntrega(Entrega entrega){
        this.entregas.add(entrega);
    }

    

    public DtTarea getDataType(){
        DtTarea toReturn = new DtTarea();

        toReturn.setDescripcion(descripcion);
        toReturn.setFechaLimite(fechaLimite);
        toReturn.setTitulo(titulo);
        toReturn.set_id(_id);
        toReturn.setEntregable(entregable);

        return toReturn;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public boolean isEntregable() {
        return entregable;
    }

    public void setEntregable(boolean entregable) {
        this.entregable = entregable;
    }

    public boolean isAutoEntregaCalculada() {
        return AutoEntregaCalculada;
    }

    public void setAutoEntregaCalculada(boolean autoEntregaCalculada) {
        AutoEntregaCalculada = autoEntregaCalculada;
    }

}