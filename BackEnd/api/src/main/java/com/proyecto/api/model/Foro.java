package com.proyecto.api.model;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.proyecto.api.dataTypes.DtForo;
import com.proyecto.api.dataTypes.DtMensaje;

@Entity
@Table(name="foros", schema = "public")
public class Foro {
    
    
    @Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int _id;

	@Column(name="titular")
    private String nombre;    

    @Column(name="fecha")
    private Date fecha;

    @Column(name="tipo")
    private String tipo;

    //////////////////////////////////////////////////// Relaciones ///////////////////////////////////

    @OneToMany(mappedBy = "foro", orphanRemoval = true)
    private List<Mensaje> mensajes;

    //////////////////////////////////////////////////// Fin de Bloque ////////////////////////////////

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }    

    public List<Mensaje> getMensajes() {
        return mensajes;
    }

    public void setMensajes(List<Mensaje> mensajes) {
        this.mensajes = mensajes;
    }

    public void addMensaje(Mensaje mensaje) {
        this.mensajes.add(mensaje);
    }

    public void removeMensaje(Mensaje mensaje) {
        this.mensajes.remove(mensaje);
    }

    public Foro() {}

    public Foro(int _id, String nombre, Date fecha, String tipo, List<Mensaje> mensajes) {
        this._id = _id;
        this.nombre = nombre;
        this.fecha = fecha;
        this.tipo = tipo;
        this.mensajes = mensajes;
    }    

    public DtForo getDataType(){
        DtForo toReturn = new DtForo();

        toReturn.setFecha(fecha);

        if (mensajes != null)
        {
            List<DtMensaje> dtm = new ArrayList<DtMensaje>();
            for(Mensaje m : mensajes){
                dtm.add(m.getDataType());
            }    
            toReturn.setMensajes(dtm);
        }
        toReturn.setNombre(nombre);
        toReturn.setTipo(tipo);
        toReturn.set_id(_id);

        return toReturn;
    }

}

