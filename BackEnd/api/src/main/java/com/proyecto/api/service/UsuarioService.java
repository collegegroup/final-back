package com.proyecto.api.service;

import java.util.ArrayList;
import java.util.List;

import com.proyecto.api.model.Email;
import com.proyecto.api.model.Inscripcion;
import com.proyecto.api.dataTypes.DtUsuario;
import com.proyecto.api.model.Mensaje;
import com.proyecto.api.model.DocenteCurso;
import com.proyecto.api.model.Usuario;
import com.proyecto.api.repository.jpa.CursoRepository;
import com.proyecto.api.repository.jpa.DocenteCursoRepository;
import com.proyecto.api.repository.jpa.EntregaRepository;
import com.proyecto.api.repository.jpa.InscripcionRepository;
import com.proyecto.api.repository.jpa.MensajeRepository;
import com.proyecto.api.repository.jpa.UsuarioRepository;
import com.proyecto.api.util.JwtTokenUtil;
import com.proyecto.api.util.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class UsuarioService {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UsuarioRepository repository;
    @Autowired
    private CursoRepository crepository;
    @Autowired
    private PasswordEncoder bcryptEncoder;
    @Autowired
    private JwtTokenUtil jwt;
    @Autowired
    private JwtUserDetailsService jwtUser;

    @Autowired
    private InscripcionRepository inscripcionRepository;
    @Autowired
    private MensajeRepository mensajeRepository;
    @Autowired
    private EntregaRepository entregaRepository;
    @Autowired
    private DocenteCursoRepository docenteCursoRepository;
    
    public int countAdmins(){
        List<DtUsuario> retorno = new ArrayList<>();
        List<Usuario> usuarios = repository.findAll();
        for(Usuario u : usuarios){
            DtUsuario temp = u.getDataTypeBase();
            if(temp.getTipoUsu() == 'A'){
                retorno.add(temp);
            }
        }
        return retorno.size();
    }

    public List<DtUsuario> getAllUsers(String token) {
        List<DtUsuario> retorno = new ArrayList<>();
        DtUsuario requestedBy = getUserByToken(token);
        if(requestedBy.getTipoUsu() == 'A'){
            List<Usuario> usuarios = repository.findAll();
            for(Usuario u : usuarios){
                retorno.add(u.getDataTypeBase());
            }
        }else{
            DtUsuario usu = new DtUsuario();
            usu.setError("No eres Administrador");
            retorno.add(usu);
        }
        return retorno;
    }

    public DtUsuario getUserById(int id, String token) {
        DtUsuario usu;
        try{
            usu = repository.findBy_id(id).getDataTypeBase();

            DtUsuario requestedBy = getUserByToken(token);

            if(requestedBy.getTipoUsu() != 'A'){
                if(!usu.getMail().equals(requestedBy.getMail())){
                    usu.setCarrera("");
                    usu.setDescripcion("");
                    usu.setDireccion("");
                    usu.setDocumento("");
                    usu.setMail("");
                    usu.setTipoDocumento("");
                }
            }
        }catch(Exception e){
            usu = new DtUsuario();
            usu.set_id(id);
            usu.setError("No existe usuario con esa ID.");
        }

        return usu;
    }

    public List<DtUsuario> getEstudiantesByCurso(int id, String token) {
        List<DtUsuario> usuarios = new ArrayList<>();
        var c = crepository.findBy_id(id);
        List <Inscripcion> inscriptos = c.getInscriptos();
        DtUsuario requestedBy = getUserByToken(token);
        for(Inscripcion i : inscriptos){
            DtUsuario usu = i.getEstudiante().getDataTypeBase();
            if(requestedBy.getTipoUsu() != 'A'){
                usu.setCarrera("");
                usu.setDescripcion("");
                usu.setDireccion("");
                usu.setDocumento("");
                usu.setMail("");
                usu.setTipoDocumento("");
            }
            usuarios.add(usu);
        }
        return usuarios;
    }

    public List<DtUsuario> getDocentesByCurso(int id, String token) {
        List<DtUsuario> usuarios = new ArrayList<>();
        var c = crepository.findBy_id(id);
        List <DocenteCurso> docentes = c.getDocentesAsignados();
        DtUsuario requestedBy = getUserByToken(token);

        for(DocenteCurso d : docentes){
            DtUsuario usu = d.getDocente().getDataTypeBase();
            if(requestedBy.getTipoUsu() != 'A'){
                usu.setCarrera("");
                usu.setDescripcion("");
                usu.setDireccion("");
                usu.setDocumento("");
                usu.setMail("");
                usu.setTipoDocumento("");
            }
            usuarios.add(usu);
        }
        return usuarios;
    }

    public List<DtUsuario> getUsuarioTipo(char tipo, String token) {
        List<DtUsuario> usuarios = new ArrayList<>();
        DtUsuario requestedBy = getUserByToken(token);

        if(requestedBy.getTipoUsu() == 'A'){
            var c = repository.findAll();
            for(Usuario d : c){
                if(d.getDataTypeBase().getTipoUsu() == tipo)
                usuarios.add(d.getDataTypeBase());
            }
        }else{
            DtUsuario usu = new DtUsuario();
            usu.setError("No eres Administrador");
            usuarios.add(usu);
        }

        return usuarios;
    }

    public DtUsuario EditarPerfil(String mailOriginal, Usuario user, String token) {
        DtUsuario toReturn = new DtUsuario();
        var u = repository.findByMail(mailOriginal);
        String requestedBy = getUserByToken(token).getMail();
        
        new Log(u.getMail(),Thread.currentThread()); //Log

        try {
            if(!requestedBy.equals(mailOriginal)){
                toReturn.setError("El Usuario que pidio la modificacion esta modificando un usuario que no es el.");
            }else{
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(mailOriginal, user.getPassword()));
            
                u.setDescripcion(user.getDescripcion());
                u.setDireccion(user.getDireccion());
                u.setDocumento(user.getDocumento());
        
                if (!user.getMail().equals(u.getMail())) {
                    try{
                        jwtUser.loadUserByUsername(user.getMail());
                        toReturn.setError("El mail nuevo que se quiere ingresar ya esta siendo usado");
                    }catch(UsernameNotFoundException ex){
                        u.setMail(user.getMail());
                    }
                }
        
                u.setNombre(user.getNombre());               
        
                u.setTipoDocumento(user.getTipoDocumento());
            }
        }catch(Exception exception){
            toReturn.setError("Contraseña Incorrecta");
        }

        if(toReturn.getError().equals("NONE")){
            toReturn = repository.save(u).getDataTypeBase();
        }

        new Log(toReturn.getError(),Thread.currentThread()); //Log

        return toReturn;
    }

    public DtUsuario EditarPerfilAdmin(String mailOriginal, Usuario user, String token) {
        DtUsuario toReturn = new DtUsuario();
        var u = repository.findByMail(mailOriginal);
        DtUsuario usu = getUserByToken(token);

        if(usu.getTipoUsu() == 'A'){
		 
            u.setDescripcion(user.getDescripcion());
            u.setDireccion(user.getDireccion());
            u.setDocumento(user.getDocumento());
    
            new Log(user.getMail(),Thread.currentThread()); //Log
            new Log(user.getDescripcion(),Thread.currentThread()); //Log
            new Log(user.getDireccion(),Thread.currentThread()); //Log
            new Log(user.getDocumento(),Thread.currentThread()); //Log

            if (!user.getMail().equals(u.getMail())) {
                try{
                    jwtUser.loadUserByUsername(user.getMail());
                    toReturn.setError("El mail nuevo que se quiere ingresar ya esta siendo usado");
                }catch(UsernameNotFoundException ex){
                    u.setMail(user.getMail());
                }
            }
    
            u.setNombre(user.getNombre());
        
            u.setTipoDocumento(user.getTipoDocumento());
        }else{
            toReturn.setError("El usuario que pidio la modificacion no es Administrador");
        }

        if(!toReturn.getError().equals("NONE")){
            return toReturn;
        }else{
            return repository.save(u).getDataTypeBase();
        }
    }

    public DtUsuario CambiarPass(String mail, String oldPass, String newPass1, String newPass2) {

        var u = repository.findByMail(mail);
        try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(mail, oldPass));
            if(newPass1.equals(newPass2)){
                
                u.setPassword(bcryptEncoder.encode(newPass1));
        
                return repository.save(u).getDataTypeBase();
            }else{
                DtUsuario dtusu = new DtUsuario();
                dtusu.setError("Las Contraseñas no Coinciden");
                return dtusu;
            }
        }catch(Exception exception){
            DtUsuario dtusu = new DtUsuario();
            dtusu.setError("Contraseña Antigua Incorrecta");
            System.out.println(exception.getMessage());
            return dtusu;
        }
    }

    public boolean RecuperarContra(String email) {

        var u = repository.findByMail(email);
        u.setPassword(bcryptEncoder.encode("Sapp.io2020"));
        repository.save(u);
        Email e = new Email();
        e.setTo(email);
        e.setFrom("noreplysapio@gmail.com");
        e.setPassword("Proyecto2020.");
        e.setSubject("Recuperación de Contraseña - Sapp.io");
        e.setBody("<html>" + "<header>"
                + "<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' integrity='sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin='anonymous'>"
                + "</header>" + "<body>" + "<h4>Oops.. parece que olvidaste tu contraseña.</h4>"
                + "<p>Cambiamos tu contraseña y ahora es: Sapp.io2020. Te recomendamos actualizarla en la sección Editar Perfil.</p>"
                + "<p>Saludos</p>" + "<p>Equipo de Sapp.io</p>" + "</body>" + "</html>");

        return EmailService.sendEmail(e);

    }

    public boolean bajaUsuario(String email, String token) {
        DtUsuario requestedBy = getUserByToken(token);

        if(requestedBy.getTipoUsu() == 'A'){
            try {
                Usuario u = repository.findByMail(email);
                DtUsuario dtu = u.getDataTypeBase();
                
                if(dtu.getTipoUsu() == 'E'){
                    inscripcionRepository.deleteInBatch(inscripcionRepository.getInscripcionesDeEstudiante(u.get_id()));
                    entregaRepository.deleteInBatch(entregaRepository.getEntregasDeEstudiante(u.get_id()));

                }else if(dtu.getTipoUsu() == 'D'){
                    docenteCursoRepository.deleteInBatch(docenteCursoRepository.getInscripcionesDeDocente(u.get_id()));
                }

                mensajeRepository.deleteInBatch(mensajeRepository.getMensajesDeUsuario(u.get_id()));

                repository.delete(u);
                return true;

            } catch (Exception e) {
                return false;
            }
        }else{
            return false;
        }

    }

    public DtUsuario getUserByToken(String token) {
        String email = jwt.getUsernameFromToken(token);
        Usuario user = repository.findByMail(email);
        return user.getDataTypeBase();
    }

}
