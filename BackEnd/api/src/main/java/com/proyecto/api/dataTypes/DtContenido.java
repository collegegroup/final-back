package com.proyecto.api.dataTypes;

public class DtContenido {
    public int _id;
    private String tipo;
    private String contenido;
    private String archivo;
    private String error = "NONE";

    public DtContenido(){}
    public DtContenido(int _id, String tipo, String contenido, String archivo) {
        this._id = _id;
        this.tipo = tipo;
        this.contenido = contenido;
        this.archivo = archivo;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
