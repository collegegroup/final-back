package com.proyecto.api.service;

import java.util.ArrayList;
import java.util.List;

import com.proyecto.api.dataTypes.DtLibro;
import com.proyecto.api.dataTypes.DtUsuario;
import com.proyecto.api.model.Contenido;
import com.proyecto.api.model.Curso;
import com.proyecto.api.model.DocenteCurso;
import com.proyecto.api.model.Libro;
import com.proyecto.api.repository.jpa.CursoRepository;
import com.proyecto.api.repository.jpa.LibroRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LibroService {

    @Autowired
    private LibroRepository repository;
    @Autowired
    private CursoRepository cursoRepository;
    @Autowired
    private UsuarioService userService;
    @Autowired
    private CursoService cursoService;
    @Autowired
    private ContenidoService contenidoService;

	public Object getAllLibros(String token) {
        DtUsuario requestedBy = userService.getUserByToken(token);
        List<DtLibro> retorno = new ArrayList<>();

        if(requestedBy.getTipoUsu() == 'A'){
            List<Libro> libros = repository.findAll();
            for (Libro l : libros) {
                retorno.add(l.getDataType());
            }
        }else{
            DtLibro temp = new DtLibro();
            temp.setError("No eres Administrador");
            retorno.add(temp);
        }
        
        return retorno;
	}

	public Object getLibroById(int id, String token) {
        DtUsuario requestedBy = userService.getUserByToken(token);
        Curso c = cursoRepository.getCursoDeLibro(id);
        
        if(cursoService.estaUsuarioEnCurso(requestedBy.getMail(), c) || requestedBy.getTipoUsu() == 'A'){
            return repository.findBy_id(id).getDataType();
        }else{
            DtLibro temp = new DtLibro();
            temp.setError("No tiene permisos para obtener los datos de este Libro");
            return temp;
        }

    }

	public Object altaLibro(int idCurso, Libro libro, String token) {
        DtUsuario requestedBy = userService.getUserByToken(token);
        var curso = cursoRepository.findBy_id(idCurso);
        DtLibro l = new DtLibro();;

        if(requestedBy.getTipoUsu() != 'E'){
            if(requestedBy.getTipoUsu() == 'A'){
                curso.getLibros().add(libro);
                repository.save(libro);
                l = libro.getDataType();

            }else if(requestedBy.getTipoUsu() == 'D'){
                List<DocenteCurso> ld = curso.getDocentesAsignados();
                boolean isDocenteofCurso = false;
                
                for(DocenteCurso dc : ld){
                    if(dc.getDocente().getMail().equals(requestedBy.getMail())){
                        isDocenteofCurso = true;
                    }
                }

                if(isDocenteofCurso){
                    curso.getLibros().add(libro);
                    repository.save(libro);
                    l = libro.getDataType();
                }else{
                    l = new DtLibro();
                    l.setError("No es docente de este curso como para dar de alta un Libro.");
                }
            }
        }else{
            l = new DtLibro();
            l.setError("No tienes permisos para dar de Alta un Libro.");
        }

        return l;
    }
    
	public Object bajaLibro(int id, String token) {
		DtUsuario requestedBy = userService.getUserByToken(token);
        Curso curso = cursoRepository.getCursoDeLibro(id);

        if((cursoService.estaUsuarioEnCurso(requestedBy.getMail(), curso) || requestedBy.getTipoUsu() == 'A') && requestedBy.getTipoUsu() != 'E'){
            try {
                var l = repository.findBy_id(id);
                for(Contenido c : l.getContenidos()){
                    contenidoService.bajaContenido(c.get_id(), l.get_id());
                }
                curso.getLibros().remove(l);
                repository.delete(l);
                return true;
            } catch (Exception e) {
                return false;
            }
        }else{
            return false;
        }
    }
    
	public Object editarLibro(int id, Libro libro, String token) {
        DtUsuario requestedBy = userService.getUserByToken(token);
        Curso c = cursoRepository.getCursoDeLibro(id);

        if((cursoService.estaUsuarioEnCurso(requestedBy.getMail(), c) || requestedBy.getTipoUsu() == 'A') && requestedBy.getTipoUsu() != 'E'){
            var l = repository.findBy_id(id);
            l.setTitulo(libro.getTitulo());
            l.setColor(libro.getColor());
        
            return repository.save(l).getDataType();
        }else{
            DtLibro l = new DtLibro();
            l.setError("No tiene permisos para editar un libro");
            return l;
        }
	}

	public Object getLibrosByCurso(int id, String token) {
		DtUsuario requestedBy = userService.getUserByToken(token);
        List<DtLibro> toReturn = new ArrayList<DtLibro>();

        if(cursoService.estaUsuarioEnCurso(requestedBy.getMail(), id) || requestedBy.getTipoUsu() == 'A'){
            Curso c = cursoRepository.findBy_id(id);
            List<Libro> lisaLibros = c.getLibros();
    
            for(Libro libro : lisaLibros){
                toReturn.add(libro.getDataType());
            }
        }else{
            DtLibro temp = new DtLibro();
            temp.setError("Usted no tiene permisos para acceder a los libros de este curso.");
            toReturn.add(temp);
        }

        return toReturn;
	}
    
}
