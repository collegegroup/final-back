package com.proyecto.api.model;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.proyecto.api.dataTypes.DtUsuario;

@Entity
@DiscriminatorValue("D")
public class Docente extends Usuario {

    //////////////////////////////////////////////////// Relaciones ///////////////////////////////////

    @OneToMany(mappedBy = "docente")
    private List<DocenteCurso> docenteCursos;

    //////////////////////////////////////////////////// Fin de Bloque ////////////////////////////////


    public List<DocenteCurso> getDocenteCursos() {
        return docenteCursos;
    }

    public void setDocenteCursos(List<DocenteCurso> docenteCursos) {
        this.docenteCursos = docenteCursos;
    }

    public Docente() {}

    public Docente(int _id, String nombre, String mail, String password, String imagen, String direccion,
            String descripcion, String links, String tipoDocumento, String documento,
            List<DocenteCurso> docenteCursos) {
        super(_id, nombre, mail, password, imagen, direccion, descripcion, links, tipoDocumento, documento);
        this.docenteCursos = docenteCursos;
    }  

    public DtUsuario getDataType(){
        DtUsuario toReturn = this.getDataTypeBase();

        toReturn.setTipoUsu('D');

        return toReturn;
    }    

}
