package com.proyecto.api.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Log {
	
	static String line=null;
    static File log = new File("logs/");
    String [] newDate = new String [3];
    static SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd - HH-mm-ss");
    SimpleDateFormat dtq = new SimpleDateFormat("dd/MM/yy-HH:mm:ss");
    Date date = new Date(System.currentTimeMillis());
    static long sysStart;
    
    public Log (String line, Thread t) {
    	setLine("["+dtq.format(date)+"]["+runTimeFormat()+"]["+t.getStackTrace()[2].getClassName().replace(".", "/")+".java][Line: "+t.getStackTrace()[2].getLineNumber()+"] - "+line);
        printer(getLine(),true);
        writer();
    }
    public Log (String line, String server){
        
        setLine("["+dtq.format(date)+"]["+runTimeFormat()+"] - "+server+" - "+line);
        printer(getLine(),true);
        writer();
        setLine("["+dtq.format(date)+"]["+runTimeFormat()+"] - "+line);
        
    }
    public Log (String line){
        
        setLine("["+dtq.format(date)+"]["+runTimeFormat()+"] - "+line);
        printer(getLine(),true);
        writer();
    }
    public Log (String line, boolean printOnCmd, int dummy){
        
        setLine("["+dtq.format(date)+"]["+runTimeFormat()+"] - "+line);
        if(printOnCmd){
            printer(getLine(),true);
        }
        writer();
    }
    public Log (String line, boolean doNextLine){
        
        //runTimeFormat();
        if(doNextLine){
            setLine(line);
        }else{
            setLine("["+dtq.format(date)+"]["+runTimeFormat()+"] - "+line);
        }
        printer(getLine(),doNextLine);
        writer();
        
    }
    public Log (String line, boolean doNextLine, boolean doStarter){
        
        //runTimeFormat();
        if(doNextLine){
            setLine(line);
        }else{
            if(doStarter){
                setLine("["+dtq.format(date)+"]["+runTimeFormat()+"] - "+line);
            }else{
                setLine(line);
            }
        }
        printer(getLine(),doNextLine);
        writer();
        
    }
    public Log (int line){
        setLine("["+dtq.format(date)+"]["+runTimeFormat()+"] - "+line);
        printer(getLine(),true);
        writer();
    }
    public Log (){
        setLine("["+dtq.format(date)+"]["+runTimeFormat()+"] - "+"This is a test");
        printer(getLine(),true);
        writer();
    }
    public String runTimeFormat (){
        String formatedMilisec,formatedSeconds,formatedMinutes,formatedH,formatedD;
        
        if(((System.currentTimeMillis()-getSysStart())%1000)<100){
            if(((System.currentTimeMillis()-getSysStart())%1000)<10){
                formatedMilisec = "00"+(System.currentTimeMillis()-getSysStart())%1000;
            }else{
                formatedMilisec = "0"+(System.currentTimeMillis()-getSysStart())%1000;
            }
        }else{
            formatedMilisec = ""+(System.currentTimeMillis()-getSysStart())%1000;
        }
        
        if((((System.currentTimeMillis()-getSysStart())/1000)%60)<10){
            formatedSeconds = "0"+((System.currentTimeMillis()-getSysStart())/1000)%60;
        }else{
            formatedSeconds = ""+((System.currentTimeMillis()-getSysStart())/1000)%60;
        }
        
        if((((System.currentTimeMillis()-getSysStart())/1000)/60%60)<10){
            formatedMinutes = "0"+((System.currentTimeMillis()-getSysStart())/1000)/60%60;
        }else{
            formatedMinutes = ""+((System.currentTimeMillis()-getSysStart())/1000)/60%60;
        }
        
        if((((System.currentTimeMillis()-getSysStart())/1000)/60/60%24)<10){
            formatedH = "0"+((System.currentTimeMillis()-getSysStart())/1000)/60/60%24;
        }else{
            formatedH = ""+((System.currentTimeMillis()-getSysStart())/1000)/60/60%24;
        }
        
        if((((System.currentTimeMillis()-getSysStart())/1000)/60/60/24)<100){
            if((((System.currentTimeMillis()-getSysStart())/1000)/60/60/24)<10){
                formatedD = "00"+((System.currentTimeMillis()-getSysStart())/1000)/60/60/24;
            }else{
                formatedD = "0"+((System.currentTimeMillis()-getSysStart())/1000)/60/60/24;
            }
        }else{
            formatedD = ""+((System.currentTimeMillis()-getSysStart())/1000)/60/60/24;
        }
        
        return formatedD+":"+formatedH+":"+formatedMinutes+":"+formatedSeconds+"."+formatedMilisec;
    }
    public void printer(String line, boolean doNextLine){
        if(doNextLine){
            System.out.println(getLine());
        }else{
            System.out.print(getLine());
        }
    }
    
    public static void writer(){
        
        if(log.exists()){}else{
            log.mkdir();
        }
        try{
            log = new File("logs/"+dtf.format(getSysStart())+".txt");
            
            try (BufferedWriter bw = new BufferedWriter (new OutputStreamWriter(new FileOutputStream(log,true), "UTF-8"))) {
                bw.write(getLine());
                bw.newLine();
                bw.close();
            }
        }catch(Exception e){
            System.out.println("Error al escribir - "+e);
        }
    }

    public static long getSysStart() {
        return sysStart;
    }
    public static void setSysStart(long sysStarts) {
        sysStart = sysStarts;
    }

    public static String getLine() {
        return line;
    }
    private void setLine(String line){
        Log.line = line;
    }
    
}
