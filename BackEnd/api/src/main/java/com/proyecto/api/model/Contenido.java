package com.proyecto.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.proyecto.api.dataTypes.DtContenido;

@Entity
@Table(name = "contenidos", schema = "public")
public class Contenido {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int _id;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "contenido", length = 10000)
    private String contenido;

    @Column(name = "archivo")
    private String archivo;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public Contenido() {
    }

    public Contenido(int _id, String tipo, String contenido, String archivo) {
        this._id = _id;
        this.tipo = tipo;
        this.contenido = contenido;
        this.archivo = archivo;
    }

    public DtContenido getDataType(){
        DtContenido toReturn = new DtContenido();

        toReturn.set_id(_id);
        toReturn.setTipo(tipo);
        toReturn.setContenido(contenido);             
        toReturn.setArchivo(archivo);

        return toReturn;
    }

}
