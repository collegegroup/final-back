package com.proyecto.api.controller;

import com.proyecto.api.model.Tarea;
import com.proyecto.api.service.TareaService;
import com.proyecto.api.util.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/tareas")
public class TareaController {

  @Autowired
  private TareaService service;

  // TESTING
  @RequestMapping(value = "/", method = RequestMethod.GET)
  public ResponseEntity<Object> getAllTareas() {
    new Log("[Inicio] Obtener Tareas",Thread.currentThread()); //Log
    ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getAllTareas(), HttpStatus.OK);
    new Log("[Fin] Obtener Tareas",Thread.currentThread()); //Log
    return toReturn;
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public ResponseEntity<Object> getTareaById(@PathVariable("id") Integer id) {
    new Log("[Inicio] Obtener Tareas por ID",Thread.currentThread()); //Log
    ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getTareaById(id), HttpStatus.OK);
    new Log("[Fin] Obtener Tareas por ID",Thread.currentThread()); //Log
    return toReturn;
  }

  // ENDPOINTS APP

  @RequestMapping(value = "/byCurso/{cursoId}", method = RequestMethod.GET)
  public ResponseEntity<Object> getTareasDeCurso(@PathVariable("cursoId") Integer cursoId) {
    new Log("[Inicio] Obtener Tareas de Curso",Thread.currentThread()); //Log
    ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getTareaDeCurso(cursoId), HttpStatus.OK);
    new Log("[Fin] Obtener Tareas de Curso",Thread.currentThread()); //Log
    return toReturn;
  }

  @RequestMapping(value = "/byUsuario/{mailUsuario}", method = RequestMethod.GET)
  public ResponseEntity<Object> getTareasDeUsuario(@PathVariable("mailUsuario") String mailUsuario) {
    new Log("[Inicio] Obtener Tareas de Usuario",Thread.currentThread()); //Log
    ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getTareaDeUsuario(mailUsuario), HttpStatus.OK);
    new Log("[Fin] Obtener Tareas de Usuario",Thread.currentThread()); //Log
    return toReturn;
  }

  @RequestMapping(value = "/altaTarea/{id}", method = RequestMethod.POST)
  public ResponseEntity<Object> altaTarea(@PathVariable("id") Integer id, @Validated @RequestBody Tarea tarea) {
    new Log("[Inicio] Alta Tarea",Thread.currentThread()); //Log
    ResponseEntity<Object> toReturn = new ResponseEntity<>(service.altaTarea(tarea, id), HttpStatus.OK);
    new Log("[Fin] Alta Tarea",Thread.currentThread()); //Log
    return toReturn;
  }

  @RequestMapping(value = "/bajaTarea/{id}/{idCurso}", method = RequestMethod.DELETE)
  public ResponseEntity<Object> bajaTarea(@PathVariable("id") Integer id, @PathVariable("idCurso") Integer idCurso) {
    new Log("[Inicio] Baja Tarea",Thread.currentThread()); //Log
    ResponseEntity<Object> toReturn = new ResponseEntity<>(service.bajaTarea(id, idCurso), HttpStatus.OK);
    new Log("[Fin] Baja Tarea",Thread.currentThread()); //Log
    return toReturn;
  }

  @RequestMapping(value = "/editarTarea/{idTarea}", method = RequestMethod.PUT)
  public ResponseEntity<Object> editarTarea(@PathVariable("idTarea") Integer idTarea, @Validated @RequestBody Tarea tarea) {
    new Log("[Inicio] Editar Tarea",Thread.currentThread()); //Log
    ResponseEntity<Object> toReturn = new ResponseEntity<>(service.editarTarea(idTarea, tarea), HttpStatus.OK);
    new Log("[Fin] Editar Tarea",Thread.currentThread()); //Log
    return toReturn;
  }

}
