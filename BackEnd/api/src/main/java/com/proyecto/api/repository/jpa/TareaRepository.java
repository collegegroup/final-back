package com.proyecto.api.repository.jpa;

import com.proyecto.api.model.Tarea;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TareaRepository extends JpaRepository<Tarea,String> {
    Tarea findBy_id(int _id);
}
