package com.proyecto.api.repository.jpa;

import com.proyecto.api.model.Foro;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ForoRepository extends JpaRepository<Foro,String>{
    Foro findBy_id(int _id);
}
