package com.proyecto.api.repository.jpa;

import com.proyecto.api.model.Libro;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LibroRepository extends JpaRepository<Libro,String> {
    Libro findBy_id(int _id);
    Libro findByTitulo(String titulo);
}
