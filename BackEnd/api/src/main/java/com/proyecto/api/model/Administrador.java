package com.proyecto.api.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.proyecto.api.dataTypes.DtUsuario;

@Entity
@DiscriminatorValue("A")
public class Administrador extends Usuario {

    public Administrador() {}

    public Administrador(int _id, String nombre, String mail, String password, String imagen, String direccion,
            String descripcion, String links, String tipoDocumento, String documento) {
        super(_id, nombre, mail, password, imagen, direccion, descripcion, links, tipoDocumento, documento);
    }
    
    public DtUsuario getDataType(){
        DtUsuario toReturn = this.getDataTypeBase();

        toReturn.setTipoUsu('A');

        return toReturn;
    }

}
