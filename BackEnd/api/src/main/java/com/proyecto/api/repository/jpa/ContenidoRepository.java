package com.proyecto.api.repository.jpa;

import com.proyecto.api.model.Contenido;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContenidoRepository extends JpaRepository<Contenido,String> {
    Contenido findBy_id(int _id);
}