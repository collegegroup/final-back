package com.proyecto.api.controller;

import java.util.Map;

import com.proyecto.api.model.Mensaje;
import com.proyecto.api.service.MensajeService;
import com.proyecto.api.util.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/mensajes")
public class MensajeController {

  @Autowired
  private MensajeService service;

  @RequestMapping(value = "/", method = RequestMethod.GET)
  public ResponseEntity<Object> getAllMensajes() {
    new Log("[Inicio] Obtener Mensajes",Thread.currentThread()); //Log
    ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getAllMensajes(), HttpStatus.OK);
    new Log("[Fin] Obtener Mensajes",Thread.currentThread()); //Log
    return toReturn;
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public ResponseEntity<Object> getMensajeById(@PathVariable("id") Integer id) {
    new Log("[Inicio] Obtener Mensaje por ID",Thread.currentThread()); //Log
    ResponseEntity<Object> toReturn = new ResponseEntity<>(service.getMensajeById(id), HttpStatus.OK);
    new Log("[Fin] Obtener Mensaje por ID",Thread.currentThread()); //Log
    return toReturn;
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
  public ResponseEntity<Object> modifyMensajeById(@PathVariable("id") Integer id, @Validated @RequestBody Mensaje mensaje) {
    new Log("[Inicio] Editar Mensaje",Thread.currentThread()); //Log
    ResponseEntity<Object> toReturn = new ResponseEntity<>(service.modifyMensajeById(id, mensaje), HttpStatus.OK);
    new Log("[Fin] Editar Mensaje",Thread.currentThread()); //Log
    return toReturn;
  }

  @RequestMapping(value = "/deForo/{foroId}", method = RequestMethod.POST)
  public ResponseEntity<Object> mensajesDeForo(@PathVariable("foroId") int foroId, @RequestHeader Map<String,String> headers) {
    new Log("[Inicio] Obtener Mensajes de Foro",Thread.currentThread()); //Log
    String token = headers.get("authorization").substring(7);
    ResponseEntity<Object> toReturn = new ResponseEntity<>(service.mensajesDeForo(foroId, token), HttpStatus.OK);
    new Log("[Fin] Obtener Mensajes de Foro",Thread.currentThread()); //Log
    return toReturn;
  }

  @RequestMapping(value = "/altaMensaje/{foroId}/{usuarioId}", method = RequestMethod.POST)
  public ResponseEntity<Object> createMensaje(@PathVariable("foroId") Integer foroId, @PathVariable("usuarioId") Integer usuarioId,@Validated @RequestBody Mensaje mensaje) {
    new Log("[Inicio] Alta Mensaje",Thread.currentThread()); //Log
    ResponseEntity<Object> toReturn = new ResponseEntity<>(service.altaMensaje(mensaje, foroId, usuarioId), HttpStatus.OK);
    new Log("[Fin] Alta Mensaje",Thread.currentThread()); //Log
    return toReturn;
  }

  @RequestMapping(value = "/bajaMensaje/{id}/{idForo}", method = RequestMethod.DELETE)
  public ResponseEntity<Object> bajaMensaje(@PathVariable("id") Integer id, @PathVariable("idForo") Integer idForo) {
    new Log("[Inicio] Baja Mensaje",Thread.currentThread()); //Log
    ResponseEntity<Object> toReturn = new ResponseEntity<>(service.bajaMensaje(id,idForo), HttpStatus.OK);
    new Log("[Fin] Baja Mensaje",Thread.currentThread()); //Log
    return toReturn;
  }

}