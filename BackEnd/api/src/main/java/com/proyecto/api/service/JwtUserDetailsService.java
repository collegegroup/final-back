package com.proyecto.api.service;

import java.util.ArrayList;
import java.util.List;

import com.proyecto.api.dataTypes.DtUsuario;
import com.proyecto.api.model.Administrador;
import com.proyecto.api.model.Docente;
import com.proyecto.api.model.Email;
import com.proyecto.api.model.Estudiante;
import com.proyecto.api.model.Usuario;
import com.proyecto.api.repository.jpa.UsuarioRepository;
import com.proyecto.api.util.CSVHelper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
	private UsuarioService service;
	
	@Autowired
	private UsuarioRepository repository;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Usuario user = repository.findByMail(email);
		if (user == null) {
			throw new UsernameNotFoundException("Usuario no encontrado con email: " + email);
		}
		return new org.springframework.security.core.userdetails.User(user.getMail(), user.getPassword(),
				new ArrayList<>());
	}

	//CARGA MASIVA

	public List<DtUsuario> saveMasivo(String file, String token) {
		List<DtUsuario> retorno;
		try {
			DtUsuario requestedBy = service.getUserByToken(token);
			if(requestedBy.getTipoUsu() == 'A'){
				List<Usuario> usuarios = CSVHelper.deCSVaUsuarios(file);
				repository.saveAll(usuarios);

				retorno = new ArrayList<DtUsuario>();
				for (Usuario u : usuarios){
					retorno.add(u.getDataTypeBase());
				}

				return retorno;
			}else{
				retorno = new ArrayList<DtUsuario>();
				DtUsuario usu = new DtUsuario();
				usu.setError("No eres Administrador");
				retorno.add(usu);
				return retorno;
			}
		} catch (Exception e) {
			throw new RuntimeException("Error al guardar el CSV: " + e.getMessage());
		}
	}

	public DtUsuario save(Estudiante user, String token) {
		try{
			loadUserByUsername(user.getMail().toLowerCase());
			DtUsuario toReturn = new DtUsuario();
			toReturn.setError("Usuario ya existe");
			return toReturn;
		}catch(UsernameNotFoundException ex){
			DtUsuario requestedBy = service.getUserByToken(token);

			if(requestedBy.getTipoUsu() == 'A'){
				Estudiante newUser = new Estudiante();
				newUser.setNombre(user.getNombre());
				newUser.setMail(user.getMail().toLowerCase());
				newUser.setPassword(bcryptEncoder.encode("Sapp.io2020"));
				newUser.setImagen(user.getImagen());
				newUser.setDireccion(user.getDireccion());
				newUser.setDescripcion(user.getDescripcion());
				newUser.setTipoDocumento(user.getTipoDocumento());
				newUser.setDocumento(user.getDocumento());
				newUser.setCarrera(user.getCarrera());

				Email e = new Email();
				e.setTo(user.getMail().toLowerCase());
				e.setFrom("noreplysapio@gmail.com");
				e.setPassword("Proyecto2020.");
				e.setSubject("Bienvenido - Sapp.io");
				e.setBody("<html>" + "<header>"
						+ "<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' integrity='sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin='anonymous'>"
						+ "</header>" + "<body>" + "<h4>Bienvenido a Sapp.io.</h4>"
						+ "<p>Tu contraseña es: Sapp.io2020. Te recomendamos actualizarla en la sección Editar Perfil.</p>"
						+ "<p>Saludos</p>" + "<p>Equipo de Sapp.io</p>" + "</body>" + "</html>");

				EmailService.sendEmail(e);
				return repository.save(newUser).getDataType();
			}else{
				DtUsuario usu = new DtUsuario();
				usu.setError("No eres Administrador");
				return usu;
			}
		}
	}

	public DtUsuario save(Docente user, String token) {
		try{
			loadUserByUsername(user.getMail().toLowerCase());
			DtUsuario toReturn = new DtUsuario();
			toReturn.setError("Usuario ya existe");
			return toReturn;
		}catch(UsernameNotFoundException ex){
			DtUsuario requestedBy = service.getUserByToken(token);

			if(requestedBy.getTipoUsu() == 'A'){
				Docente newUser = new Docente();
				newUser.setNombre(user.getNombre());
				newUser.setMail(user.getMail().toLowerCase());
				newUser.setPassword(bcryptEncoder.encode("Sapp.io2020"));
				newUser.setImagen(user.getImagen());
				newUser.setDireccion(user.getDireccion());
				newUser.setDescripcion(user.getDescripcion());
				newUser.setTipoDocumento(user.getTipoDocumento());
				newUser.setDocumento(user.getDocumento());

				Email e = new Email();
				e.setTo(user.getMail().toLowerCase());
				e.setFrom("noreplysapio@gmail.com");
				e.setPassword("Proyecto2020.");
				e.setSubject("Bienvenido - Sapp.io");
				e.setBody("<html>" + "<header>"
						+ "<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' integrity='sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin='anonymous'>"
						+ "</header>" + "<body>" + "<h4>Bienvenido a Sapp.io.</h4>"
						+ "<p>Tu contraseña es: Sapp.io2020. Te recomendamos actualizarla en la sección Editar Perfil.</p>"
						+ "<p>Saludos</p>" + "<p>Equipo de Sapp.io</p>" + "</body>" + "</html>");

				EmailService.sendEmail(e);

				return repository.save(newUser).getDataType();
			}else{
				DtUsuario usu = new DtUsuario();
				usu.setError("No eres Administrador");
				return usu;
			}
		}
	}

	public DtUsuario save(Administrador user, String token) {
		try{
			loadUserByUsername(user.getMail().toLowerCase());
			DtUsuario toReturn = new DtUsuario();
			toReturn.setError("Usuario ya existe");
			return toReturn;
		}catch(UsernameNotFoundException ex){
			DtUsuario requestedBy;
			if(service.countAdmins() == 0){
				requestedBy = new DtUsuario();
				requestedBy.setTipoUsu('A');
			}else{
				requestedBy = service.getUserByToken(token);
			}

			if(requestedBy.getTipoUsu() == 'A'){
				Administrador newUser = new Administrador();
				newUser.setNombre(user.getNombre());
				newUser.setMail(user.getMail().toLowerCase());
				newUser.setPassword(bcryptEncoder.encode("Sapp.io2020"));
				newUser.setImagen(user.getImagen());
				newUser.setDireccion(user.getDireccion());
				newUser.setDescripcion(user.getDescripcion());
				newUser.setTipoDocumento(user.getTipoDocumento());
				newUser.setDocumento(user.getDocumento());
				Email e = new Email();
				e.setTo(user.getMail().toLowerCase());
				e.setFrom("noreplysapio@gmail.com");
				e.setPassword("Proyecto2020.");
				e.setSubject("Bienvenido - Sapp.io");
				e.setBody("<html>" + "<header>"
						+ "<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' integrity='sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin='anonymous'>"
						+ "</header>" + "<body>" + "<h4>Bienvenido a Sapp.io.</h4>"
						+ "<p>Tu contraseña es: Sapp.io2020. Te recomendamos actualizarla en la sección Editar Perfil.</p>"
						+ "<p>Saludos</p>" + "<p>Equipo de Sapp.io</p>" + "</body>" + "</html>");

				EmailService.sendEmail(e);
				return repository.save(newUser).getDataType();
			}else{
				DtUsuario usu = new DtUsuario();
				usu.setError("No eres Administrador");
				return usu;
			}
		}
	}
}