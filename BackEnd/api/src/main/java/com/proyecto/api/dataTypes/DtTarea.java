package com.proyecto.api.dataTypes;

import java.util.Date;

public class DtTarea {

    private int _id;
    private Date fechaLimite;
    private String titulo;
    private String descripcion;
    private boolean entregable;
    private String error = "NONE";
    
    public DtTarea(){}
    
    public DtTarea(int _id, Date fechaLimite, String titulo, String descripcion, boolean entregable) {
        this._id = _id;
        this.fechaLimite = fechaLimite;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.entregable = entregable;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public Date getFechaLimite() {
        return fechaLimite;
    }

    public void setFechaLimite(Date fechaLimite) {
        this.fechaLimite = fechaLimite;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isEntregable() {
        return entregable;
    }

    public void setEntregable(boolean entregable) {
        this.entregable = entregable;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    

}