package com.proyecto.api.repository.jpa;

import java.util.List;

import com.proyecto.api.model.Mensaje;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface MensajeRepository extends JpaRepository<Mensaje,String> {

    Mensaje findBy_id(int _id);

    @Query(
        value = "SELECT * FROM mensajes AS m WHERE m._usuarioid = :idUsuario", 
        nativeQuery = true)
    List<Mensaje> getMensajesDeUsuario(@Param("idUsuario") int idUsuario);

}
