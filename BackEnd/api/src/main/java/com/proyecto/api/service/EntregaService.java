package com.proyecto.api.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.proyecto.api.dataTypes.DtEntrega;
import com.proyecto.api.dataTypes.DtEntregaMinimal;
import com.proyecto.api.dataTypes.DtUsuario;
import com.proyecto.api.model.Curso;
import com.proyecto.api.model.Entrega;
import com.proyecto.api.model.Estudiante;
import com.proyecto.api.model.Inscripcion;
import com.proyecto.api.model.Tarea;
import com.proyecto.api.model.Usuario;
import com.proyecto.api.repository.jpa.CursoRepository;
import com.proyecto.api.repository.jpa.EntregaRepository;
import com.proyecto.api.repository.jpa.TareaRepository;
import com.proyecto.api.repository.jpa.UsuarioRepository;
import com.proyecto.api.util.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EntregaService {

    @Autowired
    private EntregaRepository repository;

    @Autowired
    private UsuarioRepository estudianteRepo;

    @Autowired
    private UsuarioService userService;

    @Autowired
    private TareaRepository tareaRepo;

    @Autowired
    private CursoRepository Crepository;

    public List<DtEntregaMinimal> getAllEntregas() {
        List<DtEntregaMinimal> retorno = new ArrayList<>();
        List<Entrega> entregas = repository.findAll();
        for (Entrega e : entregas) {
            retorno.add(e.getMinimalDataType());
        }
        return retorno;
    }

    public DtEntregaMinimal getEntregaById(int id) {
        return repository.findBy_id(id).getMinimalDataType();
    }

    public DtEntregaMinimal editarEntrega(int id, Entrega entrega) {
        var e = repository.findBy_id(id);
        e.setContenido(entrega.getContenido());
        e.setNota(entrega.getNota());
        e.setFechaDeEntrega(entrega.getFechaDeEntrega());

        return repository.save(e).getMinimalDataType();
    }

    public DtEntregaMinimal altaEntrega(String linkEntrega, String mailEstudiante, Integer tareaId, String token) {
        DtEntregaMinimal toReturn;

        Usuario u = (Usuario) estudianteRepo.findByMail(mailEstudiante);
        DtUsuario user = u.getDataTypeBase();
        DtUsuario requestedBy = userService.getUserByToken(token);

        if (requestedBy.getMail().equals(user.getMail()) || requestedBy.getTipoUsu() == 'A') {
            Estudiante estudiante = (Estudiante) u;

            Entrega entrega = new Entrega();
            Tarea tarea = tareaRepo.findBy_id(tareaId);

            entrega.setEstudiante(estudiante);
            entrega.setFechaDeEntrega(Calendar.getInstance().getTime());
            entrega.setNota(-1);
            entrega.setTarea(tarea);

            if (tarea.isEntregable()) {
                entrega.setArchivo(linkEntrega);
            } else {
                entrega.setArchivo("");
            }

            estudiante.addEntrega(entrega);
            entrega.setEstudiante(estudiante);

            tarea.addEntrega(entrega);
            entrega.setTarea(tarea);

            toReturn = repository.save(entrega).getMinimalDataType();

        } else {
            toReturn = new DtEntregaMinimal();
            toReturn.setError("Usted no tiene permisos para realizar una entrega en ese usuario");
        }

        return toReturn;
    }

    public boolean bajaEntrega(int id) {
        try {
            Entrega entrega = repository.findBy_id(id);

            var tarea = tareaRepo.findBy_id(entrega.getTarea().get_id());
            tarea.getEntregas().remove(entrega);

            var estudiante = (Estudiante) estudianteRepo.findByMail(entrega.getEstudiante().getMail());
            estudiante.removeEntrega(entrega);

            repository.delete(entrega);
            return true;

        } catch (Exception e) {
            return false;
        }

    }

    public DtEntrega calificarEntrega(int id, int nota) {
        Entrega entrega = repository.findBy_id(id);
        entrega.setNota(nota);
        return repository.save(entrega).getDataType();
    }

    public Object byUsuario(String mailUsuario) {
        Estudiante user = (Estudiante) estudianteRepo.findByMail(mailUsuario);
        List<DtEntregaMinimal> toReturn = new ArrayList<>();

        for (Entrega e : user.getEntregas()) {
            toReturn.add(e.getMinimalDataType());
        }

        if (user.getEntregas().size() == 0) {
            DtEntregaMinimal entrega = new DtEntregaMinimal();
            entrega.setError("No tiene entregas");
            toReturn.add(entrega);
        }

        return toReturn;
    }

    public Object byCurso(int idCurso) {
        List<DtEntrega> retorno = new ArrayList<>();
        Curso curso = Crepository.findBy_id(idCurso);

        try {
            for (Tarea t : curso.getTareas()) {
                for (Entrega e : t.getEntregas()) {
                    retorno.add(e.getDataType());
                }
            }
        } catch (Exception e) {
            // devuelvo array vacio
            System.out.println("Error: " + e);
        }

        if (retorno.size() == 0) {
            DtEntrega temp = new DtEntrega();
            temp.setError("No hay entregas");
        }

        return retorno;
    }

    public Object getEntregaByIdTarea(int idTarea) {
        List<DtEntregaMinimal> retorno = new ArrayList<>();
        Tarea tarea = tareaRepo.findBy_id(idTarea);
        checkFechaLimite(tarea);

        try {
            for (Entrega e : tarea.getEntregas()) {
                retorno.add(e.getMinimalDataType());
            }
        } catch (Exception e) {
            // devuelvo array vacio
            new Log("Error: " + e, Thread.currentThread()); // Log
        }

        if (retorno.size() == 0) {
            DtEntrega temp = new DtEntrega();
            temp.setError("No hay entregas");
        }

        return retorno;
    }

    public Object getEntregaSinCalificarByIdTarea(int idTarea) {
        List<DtEntregaMinimal> retorno = new ArrayList<>();
        Tarea tarea = tareaRepo.findBy_id(idTarea);
        checkFechaLimite(tarea);

        try {
            for (Entrega e : tarea.getEntregas()) {
                if (e.getNota() == -1) {
                    retorno.add(e.getMinimalDataType());
                }
            }
        } catch (Exception e) {
            // devuelvo array vacio
            new Log("Error: " + e, Thread.currentThread()); // Log
        }

        if (retorno.size() == 0) {
            DtEntrega temp = new DtEntrega();
            temp.setError("No hay entregas");
        }

        return retorno;
    }

    private void checkFechaLimite(Tarea t) {

        if (!t.isAutoEntregaCalculada()) {
            if (t.getFechaLimite().getTime() < Calendar.getInstance().getTime().getTime()) {
                Curso c = Crepository.getCursoDeTarea(t.get_id());
                List<Inscripcion> estudiantes = c.getInscriptos();

                List<Entrega> entregas = t.getEntregas();
                for(Inscripcion ins : estudiantes){
                    
                    int i = 0;
                    boolean found = false;
                    while(i < entregas.size() && !found){
                        if(entregas.get(i).getEstudiante().getMail().equals(ins.getEstudiante().getMail())){
                            found = true;
                        }
                        i++;
                    }

                    if(!found){
                        Entrega e = new Entrega();
                        e.setArchivo("");
                        e.setContenido("");
                        e.setEstudiante(ins.getEstudiante());
                        e.setFechaDeEntrega(Calendar.getInstance().getTime());
                        e.setTarea(t);
                        if(t.isEntregable()){
                            e.setNota(0);
                        }else{
                            e.setNota(-1);
                        }
                        repository.save(e);
                    }
                }

                t.setAutoEntregaCalculada(true);
                tareaRepo.save(t);
            }
        }

    }

}
