package com.proyecto.api.dataTypes;

public class DtLibro {

    private int _id;
    private String titulo;
    private String color;
    private String error = "NONE";

    public DtLibro(){}
    public DtLibro(int _id, String titulo, String color) {
        this._id = _id;
        this.titulo = titulo;
        this.color = color;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
