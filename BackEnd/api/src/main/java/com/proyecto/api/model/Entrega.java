package com.proyecto.api.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.proyecto.api.dataTypes.DtEntrega;
import com.proyecto.api.dataTypes.DtEntregaMinimal;

@Entity
@Table(name = "entregas", schema = "public")
public class Entrega {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int _id;

    @ManyToOne
    @JoinColumn(name = "_estudianteid")
    @JsonIgnoreProperties("entregas")
    private Estudiante estudiante;

    @ManyToOne
    @JoinColumn(name = "_tareaid")
    @JsonIgnoreProperties("entregas")
    private Tarea tarea;

    @Column(name = "fechaDeEntrega")
    private Date fechaDeEntrega;

    @Column(name="nota")
    private Integer nota;

    @Column(name="contenido")
    private String contenido;

    @Column(name="archivo",length = 8192)
    private String archivo;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    public Tarea getTarea() {
        return tarea;
    }

    public void setTarea(Tarea tarea) {
        this.tarea = tarea;
    }

    public Date getFechaDeEntrega() {
        return fechaDeEntrega;
    }

    public void setFechaDeEntrega(Date fechaDeEntrega) {
        this.fechaDeEntrega = fechaDeEntrega;
    }

    public Integer getNota() {
        return nota;
    }

    public void setNota(Integer nota) {
        this.nota = nota;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public Entrega() {}

    public Entrega(int _id, Estudiante estudiante, Tarea tarea, Date fechaDeEntrega, Integer nota, String contenido,
            String archivo) {
        this._id = _id;
        this.estudiante = estudiante;
        this.tarea = tarea;
        this.fechaDeEntrega = fechaDeEntrega;
        this.nota = nota;
        this.contenido = contenido;
        this.archivo = archivo;
    }

    public DtEntrega getDataType(){
        DtEntrega toReturn = new DtEntrega();

        toReturn.setArchivo(archivo);
        toReturn.setContenido(contenido);
        toReturn.setEstudiante(estudiante.getDataType());
        toReturn.setFechaDeEntrega(fechaDeEntrega);
        toReturn.setNota(nota);
        toReturn.setTarea(tarea.getDataType());
        toReturn.set_id(_id);

        return toReturn;
    }

    public DtEntregaMinimal getMinimalDataType(){
        DtEntregaMinimal toReturn = new DtEntregaMinimal();
        
        toReturn.setId(_id);
        toReturn.setIdTarea(tarea.get_id());
        toReturn.setLinkEntrega(archivo);
        toReturn.setMailUsuario(estudiante.getMail());
        toReturn.setNota(nota);

        return toReturn;
    }

}
