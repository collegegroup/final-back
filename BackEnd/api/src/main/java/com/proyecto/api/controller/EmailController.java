package com.proyecto.api.controller;

import com.proyecto.api.model.Email;
import com.proyecto.api.service.EmailService;
import com.proyecto.api.util.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/sendEmail")
public class EmailController {
    
    @Autowired
	public EmailController() {
		
	}
	
	@CrossOrigin
	@RequestMapping(value = "/",method = RequestMethod.POST)
	public ResponseEntity<Object> sendEmail(@RequestBody Email e) {	
        new Log("[Inicio] Envio Mail",Thread.currentThread()); //Log
		ResponseEntity<Object> toReturn = new ResponseEntity<>(EmailService.sendEmail(e), HttpStatus.OK);
        new Log("[Fin] Envio Mail",Thread.currentThread()); //Log
		return toReturn;
	}
}
