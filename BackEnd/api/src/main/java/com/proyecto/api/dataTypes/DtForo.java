package com.proyecto.api.dataTypes;

import java.util.Date;
import java.util.List;

public class DtForo {

	public int _id;
    private String nombre;    
    private Date fecha;
    private String tipo;
    private List<DtMensaje> mensajes;

    public DtForo(){}
    
    public DtForo(int _id, String nombre, Date fecha, String tipo, List<DtMensaje> mensajes) {
        this._id = _id;
        this.nombre = nombre;
        this.fecha = fecha;
        this.tipo = tipo;
        this.mensajes = mensajes;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public List<DtMensaje> getMensajes() {
        return mensajes;
    }

    public void setMensajes(List<DtMensaje> mensajes) {
        this.mensajes = mensajes;
    }

}
